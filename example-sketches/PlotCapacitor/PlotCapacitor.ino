// *** ASPETTATE A PROVARLO! lo facciamo insieme in lab ***

// elaborazione di https://www.arduino.cc/en/Tutorial/CapacitanceMeter

#define ANALOG_PIN      A0         // analog pin for measuring capacitor voltage
#define CHARGE_PIN      D0         // pin to charge the capacitor - connected to one end of the charging resistor

// ho semplificato l'esempio originale, non usa più due resistenze...
//#define chargeResistor  14700.0F   // change this to whatever resistor value you are using
//#define dischargeResistor  14700.0F   // change this to whatever resistor value you are using
#define RESISTOR  14700.0F   // change this to whatever resistor value you are using

#define PLOT
//#define CALC
#define DELAY 100
//#define DELAY 10

#define COMPLETELY_DISCHARGED 10
#define PARTIALLY_DISCHARGED 238		// 63.2% del PARTIALLY_CHARGED
#define PARTIALLY_CHARGED 648				// 63.2% (RC) => 648
#define COMPLETELY_CHARGED 900

unsigned long startCharge;
unsigned long endCharge;

boolean charge=false;
float v=0;
float pre_v=0;

void charge_mode() {
    charge=true;

    digitalWrite(LED_BUILTIN,LOW);

    pinMode(CHARGE_PIN, OUTPUT);     // set CHARGE_PIN to output
    digitalWrite(CHARGE_PIN, HIGH);  // set CHARGE_PIN HIGH and capacitor charging

    startCharge=millis();
}

void discharge_mode() {
    charge=false;

    digitalWrite(LED_BUILTIN,HIGH);

    digitalWrite(CHARGE_PIN, LOW);             // set charge pin to  LOW

    endCharge=millis();
}

void setup() {
    pinMode(LED_BUILTIN,OUTPUT);
    Serial.begin(115200);
    Serial.println("booted");
    charge_mode(); // start in "unsafe" mode
}

void voltage() {
    pre_v=v;
    v=analogRead(ANALOG_PIN);
}

/** calcola uF conoscendo il tempo e la resistenza
 */
float microFarads(int milliseconds, float resistor) {
    /* RC=T
     * C(Farad)=T(s)/R(ohm)
     */
    return 1000*milliseconds/resistor;
}

void loop() {
    //startTime = millis();
    voltage();

#ifdef PLOT
    Serial.println(v);
#endif

    delay(DELAY);

    if(v<=COMPLETELY_DISCHARGED) {
        charge_mode();
    }

    if(v>=COMPLETELY_CHARGED) {
        discharge_mode();
    }


#ifdef CALC
    if(v<=PARTIALLY_DISCHARGED && !charge) {
        // ero in discharge, calcolo RC di scarica
        Serial.print("Capacitor (discharge): ");
        Serial.println(microFarads(abs(endCharge-startCharge),RESISTOR));
    }

    if(v>=PARTIALLY_CHARGED && charge) {
        // ero in charge, calcolo RC di carica
        discharge_mode();
        Serial.print("Capacitor (charge): ");
        Serial.println(microFarads(abs(endCharge-startCharge),RESISTOR));
    }
#endif
}
