# Disclaimer

Questo file si pone come un contenitore del percorso parallelo che io (ed eventualmente altri) svilupperò durante questo corso. 
L'idea è di creare un mapping tra i concetti visti a lezione/laboratorio verso il "mondo" MicroPython.

Il presente testo non deve essere considerato come una bibbia (ci mancherebbe), piuttosto una documentazione in evoluzione che sicuramente conterrà diversi errori e per questo motivo subirà svariati aggiornamenti.



# Introduzione

### Cos'è MicroPython

MicroPython, come facilmente deducibile dal nome, è un porting del linguaggio di programmazione Python nel mondo dei micro-controllori e quindi dei sistemi embedded. 
Eredita la filosofia e la sintassi di Python 3; perciò si basa su una sintassi nella maggior parte dei casi molto semplice ed un livello di astrazione piuttosto elevato. 

### Come funziona

Solitamente le schede che supportano questo linguaggio nel cuore hanno un ESP32, tuttavia per ogni scheda è necessario flashare un firmware apposito.

All'accensione, il bootloader carica il file "boot.py" che può essere modificato a piacere; ad esempio può contenere l'intera logica di funzionamento, oppure può essere utilizzato come chiamante di un altro file .py presente in memoria.

### Svantaggi

- Non supporta le librerie standard di Python, ma ha una propria lista disponibile [qui](https://github.com/micropython/micropython-lib)
- Alcune schede hanno librerie o metodi specifici
- Non è prestante quanto C/C++
- Non esiste una toolchain ben definita come quella Arduino

### Vantaggi

- Rappresenta un'ottima soluzione per quanto riguarda prestazioni/astrazione
- Ereditando gran parte del cuore di Python, l'interazione con internet è molto semplice
- Il livello di astrazione e il concetto di "oggetto", permettono la realizzazione di progetti anche molto ampi con non troppe difficoltà

Una considerazione sulla community è d'obbligo:

Sicuramente non è vasta quanto quella che c'è intorno ad Arduino (inteso come hardware e software), tuttavia si può facilmente attingere da quest'ultima. Inoltre ci sono costanti aggiornamenti delle librerie ed esempi.





## TODO (non vincolante)
* architettura REPL
* filesystem dell'esp8266/32
* workflow di sviluppo
* firmware "interprete" da flashare, ce ne sono varie versioni? che licenza hanno? si possono autoprodurre? (build)
* citare altre piattaforme (es. MicroBIT - te lo posso prestare per fare testing, io al momento ce l'ho su uno scaffale)
