# Diario lezioni

Nota bene: a lezione non è detto si riesca ad affrontare tutti gli argomenti, ma per l'esame vanno conosciuti i capitoli del libro elencati nel README.

## Lezioni (in ordine cronologico inverso)

### Future

Le date "future" cambiano (riprogrammazione in funzione di eventi vari) man mano che andiamo avanti col corso.

CORSO TERMINATO


### Passate

 * 2019-06-14 (Δ), (lab) discussione idee progetti, *a seguire (nel pomeriggio) esame* (se ci sono candidati), FINE CORSO!!!
 * 2019-06-10 (Ω), verifica charlieplexing, CS (Chip Select), motori (ponte H), multiplexing, shift register
 * 2019-06-07 (Δ), workshop saldatura (realizziamo esempio a 6 LED da https://en.wikipedia.org/wiki/Charlieplexing)
 * 2019-06-03 (Ω), protocolli comunicazione "wired" (seriale, i2c, firmata, bit banging), attuatori (knob)
 * 2019-05-31 (Δ), ambiente Raspberry+ArduinoIDE (studenti)
 * (2019-05-27), SOSPENSIONE lezioni (post elettorale)
 * 2019-05-24 (Δ), (Alexjan Carraturo) "Embedded Systems"
 * 2019-05-20 (Ω), protocolli comunicazione "rete" (es. mqtt, osc)
 * (2019-05-17), NON c'è lezione
 * 2019-05-13 (Ω), controlli automatici, PID (esercizio con lib)
 * 2019-05-10 (Δ), Intervento di ST
 * 2019-05-06 (Ω), RotorTasker (con TaskScheduler) e intro esigenza controlli automatici
 * 2019-05-03 (Δ), (lab) TaskScheduler con esercizio
 * (2019-04-29), NON c'è lezione
 * (2019-04-26), vacanza accademica
 * (2019-04-22), vacanza accademica
 * (2019-04-19), vacanza accademica
 * 2019-04-15 (Ω), interrupt, motivando per confronto con sensore photoresist o photocell e reed (polled)
 * 2019-04-12 (Δ), (lab) completamento esercizio rgb, inizio esercizio polling digitalpin, cenni uso lib
 * 2019-04-08 (Ω), Intervento di BOSCH
 * 2019-04-05 (Δ), (lab) Rotor (esempio PWM dei poveri, lettura analogica, lettura pullup, reed), PWM vera con LED (eventualmente RGB), dato esercizio RGB+potenziometro
 * 2019-04-01 (Ω), forme d'onda, PWM, analogWrite
 * 2019-03-29 (Δ), (lab) I/O input (anche PULLUP - da sperimentare ancora) + analogici
 * 2019-03-25 (Ω), fine componenti (compreso transistor e c.i.), strumenti di misura, cenni sui sensori
 * 2019-03-22 (Δ), (lab) I/O inizio, collegamento componentistica
 * 2019-03-18 (Ω), componenti, specie condensatori, RC
 * 2019-03-15 (Δ), (lab) Arduino IDE, intro linguaggio
 * 2019-03-11 (Ω), richiamo sui principi, leggi di Ohm e Kirchhoff, resistenze, partitore, serie e parallelo di resistenze, interruttori, lampadine, relé
 * 2019-03-04 (Ω), intro Sistemi Embedded, architetture, monoprog e multiprog, MdT, memoria
 * 2019-03-01 (Δ), intro corso, panoramica Sistemi Embedded


## Argomenti da evadere (non in ordine di presentazione, non riusciremo a trattare tutti i topic in aula)

FIXME cfr. file lista domande
