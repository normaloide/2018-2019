# LISTA QUICK FIX / ELEMENTI UTILI

Il seguente documento è una lista di contenuti e argomenti presenti nel gruppo telegram. Le informazioni qui contenute non sono da considerarsi verità assolute, perciò verificatele sempre prima di utilizzarle/applicarle.



## NEGOZI FISICI

- [LED elettronica](https://ledelettronica.com/dovesiamo.html)

- [GBC elettronica](https://www.gbcelettronicamilano.com/contatti)



## SIMULATORI / SOFTWARE DI PROGETTAZIONE

- Everycircuit (non-open source)
- TinkerCAD (non-open source)
- KiCAD



## LINK

- [Definizione](<https://dl.espressif.com/dl/package_esp32_index.json>) ESP32 per IDE Arduino
- [Canale Youtube](<https://www.youtube.com/channel/UCJ0-OtVpF0wOKEqT2Z1HEtA>) che spiega in modo spiritoso alcuni concetti di elettronica
- [Driver](<https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers>) ESP32
- Calcolo corrente massima cavi, caduta di tensione e [standard AWG](<https://www.powerstream.com/Wire_Size.htm>)
- [DIY "Smartphone"](<https://hackaday.com/2019/04/03/phone-for-hackers-launches-a-crowdfunding-campaign/>)
- [Varicap]( https://en.wikipedia.org/wiki/Varicap)
- [Editor Markdown]([https://demo.codimd.org](https://demo.codimd.org/oRIG4LK1SsaDsvm7FtCXpA)) collaborativo ([Istanza del corso](<https://demo.codimd.org/oRIG4LK1SsaDsvm7FtCXpA>))
- [Tomografia](<https://hackaday.com/2019/04/06/tomography-through-an-infinite-grid-of-resistors/>) tramite resistenze
- Broker MQTT online gratuiti:
  - [Maqiatto]([https://www.maqiatto.com](https://www.maqiatto.com/))
  - [Mosquitto](http://test.mosquitto.org/)
  - [HacklabCormano](http://mqtt.hacklabcormano.it)
- [Bloop Festival](http://www.bloop-festival.com/) (ALERT: lo sfondo del sito può ledere la vista)



## GUIDE

- Repo che indica come [collegarsi a reti eduroam](https://github.com/martinius96/ESP32-eduroam/blob/master/README.md)
- [Controllo n switch](<https://create.arduino.cc/projecthub/najad/100-switches-in-a-single-pin-of-arduino-750c42>) tramite pin analogico
- Come [saldare](<https://www.youtube.com/watch?v=bGw4YCoaZxI>)
- [Pilotaggio LED RGB](<https://www.hackster.io/techmirtz/using-common-cathode-and-common-anode-rgb-led-with-arduino-7f3aa9>) a catodo/anodo comune
- [Gestione Interrupt](<https://lastminuteengineers.com/handling-esp32-gpio-interrupts-tutorial/>) ESP32 con Arduino
- [Utilizzo core ESP32](https://randomnerdtutorials.com/esp32-dual-core-arduino-ide/) con Arduino



## COLLEGAMENTO ESP32

- Installare python-serial (tramite package manager o pip)
- Installare esptool.py
- Collegare il cavo USB (inaspettato, vero?)

NOTA: In alcuni casi potrebbe risultare necessario forzare l'avvio in "flash mode" tramite il pulsantino di reset presente sulla scheda



## QUICK FIX

#### ERRORI DI COMUNICAZIONE/FLASH

Controllare il cavo usb utilizzato, molti hanno fili talmente sottile che la caduta di tensione su di essi causa problemi di comunicazione. Per verificare che la scheda venga rilevata dal computer; verificare che il relativo file venga creato in, solitamente, "/dev/"

NOTA: 

- Il fatto che la scheda si accenda, non significa che il cavo vada bene
- Ogni PC ha leggere differenze di alimentazione e gestione delle USB, perciò con lo stesso cavo si potrebbe riuscire a flashare da un computer e non da altri



#### DEVICE NON RICONOSCIUTO (FEDORA)

- ```shell
  sudo dnf install python  
  ```

- ```shell
  pip install --user pyserial
  ```

- ```shell
  chmod 777 /dev/ttyS0 (o ttyUSB0)
  ```

NOTA: se i passi precedenti non dovessero risolvere il problema; reinstallare Fedora



## PROBLEMI NOTI ESP32

- Documentazione ancora acerba > bisogna spesso far riferimento agli esempi
- Alcune funzioni non sono ancora implementate
- Alcune funzionalità (esempio: PWM) accessibili tramite "primitive"
- Durante l'utilizzo di scheda WiFI, Bluetooth e/o DAC, le letture degli ADC2_CH* sono inconsistenti



## CARATTERISTICHE BASE ESP32

- Corrente massima pin 3.3V: circa 1A (dipende molto dalla scheda specifica)
- Corrente massima pin 5V: solitamente 500mA
- Corrente massima GPIO di sink (entrante): 20mA
- Corrente massima GPIO di source (uscente): 12mA



## ESPERIMENTI INTERESSANTI

- Dekatron
- Led cube
- Deauther (esp8266 o esp32)



## LIMITI STANDARD USB

- USB 1 e 2: 500mA
- USB 3: 900mA

NOTA: questi sono valori degli standard, perciò alcuni prodotti possono fornire potenze maggiori rispetto a 2.5W-4.5W semplicemente non aderendo a tali standard (vedi Fast Charge, Turbo Charge, e le mille combinazioni della forma "<qualcosa> Charge")
