/******************************************************************************/
/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2015.
 * The use of this software is subject to the XDK SDK EULA
 */
/**
 *  @file        XDK_Datalogger_cc.c
 *
 * Application to log all the defined sensors on SD-Card
 * every one ms, initiated by auto reloaded Tasks(freertos)
 * 
 * ****************************************************************************/

/* module includes ********************************************************** */

/* own header files */
#include "itoafunction/itoafunction.h"
#include "minIni/minIni.h"
#include "BMA_280/BMA_280_ch.h"
#include "BMA_280/BMA_280_ih.h"
#include "BME_280/BME_280_ch.h"
#include "BME_280/BME_280_ih.h"
#include "BMG_160/BMG_160_ch.h"
#include "BMG_160/BMG_160_ih.h"
#include "BMI_160/BMI_160_ch.h"
#include "BMI_160/BMI_160_ih.h"
#include "BMM_150/BMM_150_ch.h"
#include "BMM_150/BMM_150_ih.h"
#include "MAX_44009/MAX_44009_ch.h"
#include "MAX_44009/MAX_44009_ih.h"
#include "XDK_Datalogger_ih.h"

/* system header files */
#include <BCDS_Basics.h>
#include <stdio.h>
#include <string.h>

/* additional interface header files */
#include "BSP_BoardType.h"
#include "BCDS_BSP_Button.h"
#include "BCDS_BSP_LED.h"
#include "FreeRTOS.h"
#include "timers.h"
#include "task.h"
#include "BCDS_SensorErrorType.h"
#include "BCDS_Accelerometer.h"
#include "XdkSensorHandle.h"
#include "BCDS_Assert.h"
#include "BCDS_SDCard_Driver.h"
#include "ff.h"
#include "em_cmu.h"
#include "em_usart.h"
#include "em_gpio.h"
#include "task.h"
#include "Serval_Clock.h"
#include "ctype.h"
#include "BCDS_CmdProcessor.h"
#include "BCDS_Assert.h"
#include "Serval_Ip.h"
#include "BSP_BoardType.h"
#include "BCDS_NetworkConfig.h"
#include "BCDS_WlanConnect.h"
#include "BCDS_BSP_Button.h"
#include "socket.h"

/* constant definitions ***************************************************** */
#define FORCE_MOUNT  					UINT8_C(1)    /** Macro used to define the force mount */
#define DEFAULT_LOGICAL_DRIVE           ""            /** Macro used to define the default drive */
#define CUSTLOGBUFSIZE	256
#define SAMPLE_TASK_INTERVAL_IN_MS	            1
#define SAMPLE_TASK_INTERVAL_IN_TICKS    (SAMPLE_TASK_INTERVAL_IN_MS/portTICK_PERIOD_MS)
#define SD_TASK_INTERVAL_IN_MS	                60
#define SD_TASK_INTERVAL_IN_TICKS        (SD_TASK_INTERVAL_IN_MS/portTICK_PERIOD_MS)
#define TIMESTAMP_UNIT_IN_MS	                1
#define TIMESTAMP_UNIT_IN_TICKS          (TIMESTAMP_UNIT_IN_MS/portTICK_PERIOD_MS)
#define STRINGREPLACEBUFFER	                    256
#define CSTOVERHEAD	                            486
#define CSVOVERHEAD	                            255
#define JSONOVERHEAD	                        735
#define JSONOVERHEAD_PER_SENSOR                  70
#define BMA280_MAXBYTES                          30
#define BMG160_MAXBYTES                          30
#define BMI160_MAXBYTES                          60
#define BMM150_MAXBYTES                          36
#define BME280_MAXBYTES                          30
#define MAX44009_MAXBYTES                        16
#define TIMESTAMP_MAXBYTES                       10
#define BYTESPERS                                18000 /**< max bytes per second that can be written to SD-Card*/

/**
 * Operational States the device traverses.
 */
enum OperationStates
{
    STARTED = 1, 			/**< App just started */
    INITIALIZED, 			/**< Correctly initialized */
    WAIT_FOR_SYNC,			/**< SLAVE MODE:	Waiting for Sync message from master */
	WAIT_FOR_DELAY_REQ,		/**< MASTER MODE:	Waiting for Delay Request message from slave */
	WAIT_FOR_DELAY_RESP,	/**< SLAVE MODE:	Waiting for Delay Response message from master */
	WAIT_FOR_DELAY_ACK,		/**< MASTER MODE:	Waiting for Delay Acknowledgement message from slave */
	SYNCED,					/**< Devices correctly synchronized */
	FAILED_TO_SYNC,			/**< ERROR STATE - Synchronization failed */
};
#define TRANSITION_TIMEOUT UINT32_C(5000)

/* local variables ********************************************************** */
static Retcode_T SDC_diskInitStatus = SDCARD_NOT_INITIALIZED;
int8_t newFile = 1; /**< variable to check if a new file has to be added, set by Button pressed event*/
int8_t missingFile = 0; /**< variable to check if some INI-File is found on SD-Card, set if not*/
int8_t buttoncount = 0; /**< variable to store the amount of button pressed-events, to create next filename*/
int8_t addnewfile = 0; /**< variable to check the led-status by button pressed-events*/
TCHAR customHeader[CUSTLOGBUFSIZE] = { 0 }; /**< variable to store the first line of custlog.ini*/
TCHAR custstring[CUSTLOGBUFSIZE] = { 0 }; /**< variable to store the second line of custlog.ini*/
TCHAR filename[13] = { 0 }; //"log_%02d.csv"; /**< variable to store the actual filename*/
FATFS globalmnt; /**< variable for mount process of SD-Card*/
xSemaphoreHandle ReadSensorSemaphor = NULL; /**< Semaphore to lock/unlock the Ping/Pong-Buffer*/
configuration *conf = &config; /**< Pointer to the Sensor-configuration*/
Buffer pingBuffer = { 0 };
Buffer pongBuffer = { 0 };
Buffer *ActiveBuffer; /**< Pointer to the Buffer which is filled */
Buffer *BackBuffer; /**< Pointer to the Buffer to read out data */
FIL fileObject = { 0 }; /* File objects */
int8_t closefile = 0;
uint32_t fastestSamplingRate = 0; /**< variable to store the fastest sampling rate*/
uint32_t logActive;
uint32_t minimalTicks = 0; /**< variable to store the minimal Ticks according to fastestSamplingRate*/

/* global variables ********************************************************* */
configuration config = { { 0 }, { 0 }, { 0 }, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }; /**< Object of the struct to store the sensor parameter defined in logger.ini*/

/* variable to store the configurable sensor parameter, used by sensor init*/
TCHAR bma280_bw[12] = { 0 };
TCHAR bmg160_bw[12] = { 0 };
TCHAR bmi160_accel_bw[12] = { 0 };
TCHAR bmi160_gyro_bw[12] = { 0 };
TCHAR bmm150_data[12] = { 0 };
TCHAR bme280_os[4] = { 0 };
TCHAR bme280_coeff[4] = { 0 };
TCHAR MAX44009_int[4] = { 0 };

CmdProcessor_T *AppCmdProcessorHandle;
static OperationState opState = STARTED;
static SemaphoreHandle_t transitionSemaphore = NULL;
static SemaphoreHandle_t syncDataSemaphore = NULL;
static SemaphoreHandle_t wifiRadioSemaphore = NULL;

static Ip_Address_T syncDestination = 0x00000000;
static int16_t		syncRecvSocketID = 0;
static xTaskHandle	recvTask = NULL;
static PtpPacket_t 	syncData		= {
	.type			= (PtpMessageType) 0,
	.validFields	= (uint8_t) 0,
	.syncSendTime	= (TickType_t) 0,
	.syncRecvTime	= (TickType_t) 0,
	.dreqSendTime	= (TickType_t) 0,
	.dreqRecvTime	= (TickType_t) 0,
	.dresSendTime	= (TickType_t) 0,
	.dresRecvTime	= (TickType_t) 0,
};

static uint8_t 	master = 0;
static int32_t 	adjust = 0;

/* PTP SYNC Section ***********************************************************/
static void updatePTPstructure(PtpPacket_t newPtp) {
	if (pdTRUE != xSemaphoreTake(syncDataSemaphore, TRANSITION_TIMEOUT))
	{
		printf("Failed to acquire syncData serialization semaphore \n\r ");
		transitionTo(FAILED_TO_SYNC);
	}

	syncData.type = newPtp.type;
	syncData.validFields = newPtp.validFields;

	if (newPtp.validFields & 0b10000000) {
		syncData.syncSendTime = newPtp.syncSendTime;
	}
	if (newPtp.validFields & 0b01000000) {
		syncData.syncRecvTime = newPtp.syncRecvTime;
	}
	if (newPtp.validFields & 0b00100000) {
		syncData.dreqSendTime = newPtp.dreqSendTime;
	}
	if (newPtp.validFields & 0b00010000) {
		syncData.dreqRecvTime = newPtp.dreqRecvTime;
	}
	if (newPtp.validFields & 0b00001000) {
		syncData.dresSendTime = newPtp.dresSendTime;
	}
	if (newPtp.validFields & 0b00000100) {
		syncData.dresRecvTime = newPtp.dresRecvTime;
	}

	if (pdTRUE != xSemaphoreGive(syncDataSemaphore))
	{
		printf("Release of syncData serialization semaphore FAILED \n\r ");
	}
}

static void BindSocketForUdpRecv(void) {
	SlSockAddrIn_t Addr;
	uint16_t AddrSize 	= (uint16_t) ZERO;
	int16_t SockID 		= (int16_t)  ZERO;

	/* copies the dummy data to send array , the first array element is the running counter to track the number of packets send so far*/
	Addr.sin_family 		= SL_AF_INET;
	Addr.sin_port 			= sl_Htons((uint16_t) SERVER_PORT);
	Addr.sin_addr.s_addr 	= 0;
	AddrSize 				= sizeof(SlSockAddrIn_t);

	SockID = sl_Socket(SL_AF_INET, SL_SOCK_DGRAM, (uint32_t) ZERO); /**<The return value is a positive number if successful; other wise negative*/
	if (SockID < (int16_t) ZERO)
	{
		printf("Failed to open socket \n\r ");
		transitionTo(FAILED_TO_SYNC);
		/* error case*/
		return;
	}

	sl_Bind(SockID, (SlSockAddr_t *) &Addr, AddrSize);
	syncRecvSocketID = SockID;
}
static Retcode_T wifiDisconnect(void)
{
    if (pdTRUE == xSemaphoreTake(wifiRadioSemaphore, TRANSITION_TIMEOUT) && pdTRUE == xSemaphoreTake(wifiRadioSemaphore, TRANSITION_TIMEOUT)) {
    	printf("Cannot retire Wifi radio channels \n\r ");
    	return RETCODE(RETCODE_SEVERITY_FATAL, SEMAPHORE_GIVE_ERROR);
    }
	WlanConnect_Disconnect(NULL);
	return WlanConnect_DeInit();
}


static void wifiUdpRecv(void *param1)
{
	BCDS_UNUSED(param1);
	uint8_t hasChannel = 0;
	int16_t Status;
	PtpPacket_t	recvd;
	for(;;) {
		Status = (int16_t)  ZERO;
		memset(&recvd, (uint32_t) ZERO, sizeof(recvd));

		printf("WAITING FOR INCOMING MESSAGE... ... \n\r ");

		if (hasChannel == 0) {
			if (pdTRUE != xSemaphoreTake(wifiRadioSemaphore, TRANSITION_TIMEOUT)) {
				printf("Cannot acquire WiFi Radio channel \n\r ");
				continue;
			}
			hasChannel = 1;
		}
		Status = sl_Recv(syncRecvSocketID, &recvd, sizeof(PtpPacket_t), (uint32_t) ZERO); /**<The return value is a number of characters sent;negative if not successful*/

		/*Check if 0 transmitted bytes sent or error condition*/
		if (Status <= (int16_t) ZERO)
		{
			Status = sl_Close(syncRecvSocketID);
			if (Status < 0)
			{
				printf("Error is closing socket after failing to recieve the Udp data \r\n");
				continue;
			}
			printf("Error in recieving data \r\n");
			transitionTo(FAILED_TO_SYNC);
			continue;
		}

		if (recvd.type == syncData.type) {
			/** The message came back via Broadcast */
			printf("Recieved a broadcast back, ignoring \n\r ");
			continue;
		}

		if (pdTRUE != xSemaphoreGive(wifiRadioSemaphore)) {
			printf("Cannot release WiFi Radio channel \n\r ");
			continue;
		}
		hasChannel = 0;

		printf("UDP recieving successful\r\n");

		switch ((uint8_t) recvd.type)
		{
			case SYNC:
			{
				recvd.validFields = recvd.validFields | 0b01000000;
				recvd.syncRecvTime = xTaskGetTickCount();
				updatePTPstructure(recvd);
				transitionTo(WAIT_FOR_DELAY_RESP);
			}
			break;
			case DELAY_REQ:
			{
				recvd.validFields = recvd.validFields | 0b00010000;
				recvd.dreqRecvTime = xTaskGetTickCount();
				updatePTPstructure(recvd);
				transitionTo(WAIT_FOR_DELAY_ACK);
			}
			break;
			case DELAY_RESP:
			{
				recvd.validFields = recvd.validFields | 0b00000100;
				recvd.dresRecvTime = xTaskGetTickCount();
				updatePTPstructure(recvd);
				transitionTo(SYNCED);
			}
			break;
			case DELAY_ACK:
			{
				syncData.type = DELAY_ACK;
				transitionTo(SYNCED);
			}
			break;
			default:
			{
				printf("Unrecognized Message Type recieved");
				transitionTo(FAILED_TO_SYNC);
				continue;
			}
		}
	}
}

/**
 * @brief Opening a UDP client side socket and sending data on a server port
 *
 * This function opens a UDP socket and tries to connect to a Server SERVER_IP
 * waiting on port SERVER_PORT.
 * Then the function will send periodic UDP packets to the server.
 *
 * @param[in]   *param1     generic pointer to any context data structure which will be passed to the function.
 *
 * @param[in]   port        destination port number
 *
 *
 */

void partialTearDown(void) {
	/** Stopping Recieveing Task */
	vTaskDelete(recvTask);

	/** Stop WLAN Radio */
	wifiDisconnect();

	/** Stop Listening on Button 2 */
	BSP_Button_Disable((uint32_t) BSP_XDK_BUTTON_2);
}


static void wifiUdpSend(void * param1, uint32_t msgType)
{
    BCDS_UNUSED(param1);
    SlSockAddrIn_t Addr;
    uint16_t AddrSize 	= (uint16_t) ZERO;
    int16_t SockID 		= (int16_t)  ZERO;
    int16_t Status 		= (int16_t)  ZERO;

    /* copies the dummy data to send array , the first array element is the running counter to track the number of packets send so far*/
    Addr.sin_family 		= SL_AF_INET;
    Addr.sin_port 			= sl_Htons((uint16_t) SERVER_PORT);
    Addr.sin_addr.s_addr 	= syncDestination;
    AddrSize 				= sizeof(SlSockAddrIn_t);

    SockID = sl_Socket(SL_AF_INET, SL_SOCK_DGRAM, (uint32_t) ZERO); /**<The return value is a positive number if successful; other wise negative*/
    if (SockID < (int16_t) ZERO)
    {
    	printf("Failed to open socket \n\r ");
        transitionTo(FAILED_TO_SYNC);
        /* error case*/
        return;
    }

    PtpPacket_t newPacket;

    if (pdTRUE != xSemaphoreTake(syncDataSemaphore, TRANSITION_TIMEOUT))
	{
		printf("Failed to acquire syncData serialization semaphore \n\r ");
		transitionTo(FAILED_TO_SYNC);
	}

    memcpy(&newPacket, &syncData, sizeof(newPacket));

	if (pdTRUE != xSemaphoreGive(syncDataSemaphore))
	{
		printf("Release of syncData serialization semaphore FAILED \n\r ");
	}

    switch ((uint8_t) msgType)
    {
    	case SYNC:
    	{
    		newPacket.type = SYNC;
    		newPacket.validFields = newPacket.validFields | 0b10000000;
    		newPacket.syncSendTime = xTaskGetTickCount();
    	}
    	break;
    	case DELAY_REQ:
    	{
    		newPacket.type = DELAY_REQ;
    		newPacket.validFields = newPacket.validFields | 0b00100000;
    		newPacket.dreqSendTime = xTaskGetTickCount();
    	}
    	break;
    	case DELAY_RESP:
    	{
    		newPacket.type = DELAY_RESP;
    		newPacket.validFields = newPacket.validFields | 0b00001000;
    		newPacket.dresSendTime = xTaskGetTickCount();
    	}
    	break;
    	case DELAY_ACK:
    	{
    		newPacket.type = DELAY_ACK;
    	}
    	break;
    	default:
    	{
    		printf("Unrecognized Message Type to be sent");
    		transitionTo(FAILED_TO_SYNC);
    		return;
    	}
    }

    updatePTPstructure(newPacket);

	if (pdTRUE != xSemaphoreTake(wifiRadioSemaphore, TRANSITION_TIMEOUT)) {
		printf("Cannot acquire WiFi Radio channel \n\r ");
		return;
	}
    Status = sl_SendTo(SockID, &syncData, sizeof(PtpPacket_t), (uint32_t) ZERO, (SlSockAddr_t *) &Addr, AddrSize);/**<The return value is a number of characters sent;negative if not successful*/
	if (pdTRUE != xSemaphoreGive(wifiRadioSemaphore)) {
		printf("Cannot release WiFi Radio channel \n\r ");
		return;
	}

    /*Check if 0 transmitted bytes sent or error condition*/
    if (Status <= (int16_t) ZERO)
    {
        Status = sl_Close(SockID);
        if (Status < 0)
        {
            printf("Error is closing socket after failing to send the Udp data \r\n");
            return;
        }
        printf("Error in sending data \r\n");
        transitionTo(FAILED_TO_SYNC);
        return;
    }
    Status = sl_Close(SockID);
    if (Status < 0)
    {
        printf("Error in closing socket after sending successfully sending the udp data \r\n");
        return;
    }
    printf("UDP sending successful\r\n");

    if (master == 0 && msgType == DELAY_ACK) {
	    partialTearDown();
	    dl_appInitSystem();
    }

    return;
}

/**
 * @brief        Send PTP packet.
 */
static void EnqueueDatatoWifi(PtpMessageType msgType)
{
    Retcode_T retVal = CmdProcessor_enqueue(AppCmdProcessorHandle, wifiUdpSend, NULL, (uint32_t) msgType);
    if (RETCODE_OK != retVal)
    {
        printf("Failed to Enqueue SendAccelDataoverWifi to Application Command Processor \r\n");
        transitionTo(FAILED_TO_SYNC);
    }
}



/**
 * @brief        This function used to connect to wifi n/w with provided SSID & Password
 *
 * @retval       RETCODE_OK is API success or an error.
 */
/**
 *  @brief       Transition from the current Operational State to the supplied one, if possible.
 */
static void processTransitionTo(void * param1, uint32_t state) {
	BCDS_UNUSED(param1);

	OperationState currentOpState = opState;
	Retcode_T returnValue = RETCODE_OK;

	if (state == currentOpState) {
		return;
	}

    if (pdTRUE != xSemaphoreTake(transitionSemaphore, TRANSITION_TIMEOUT))
    {
        printf("Failed to acquire transition serialization semaphore \n\r ");
        returnValue = RETCODE(RETCODE_SEVERITY_ERROR, SEMAPHORE_TIME_OUT_ERROR);
        transitionTo(FAILED_TO_SYNC);
    }

    printf("Attempting transition %u -> %u \n\r ", (unsigned int) currentOpState, (unsigned int) state);

    if (returnValue == RETCODE_OK) {
    	switch (state)
    		{
    			case STARTED:
    			{
    				/* CANNOT TRANSITION BACK TO STARTING STATE */
    				transitionTo(FAILED_TO_SYNC);
    				returnValue = RETCODE(RETCODE_SEVERITY_ERROR, RETCODE_FAILURE);
    			}
    			break;
    			case INITIALIZED:
    			{
    				switch (currentOpState)
    				{
    					case STARTED:
    					{
    						printf("STARTED -> INITIALIZED \n\r ");
    						opState = INITIALIZED;
    						returnValue = RETCODE_OK;
    					}
    					break;
    					default:
    					{
    						/* CANNOT TRANSITION BACK TO INITIAL STATE */
    						transitionTo(FAILED_TO_SYNC);
    						returnValue = RETCODE(RETCODE_SEVERITY_ERROR, RETCODE_FAILURE);
    					}
    					break;
    				}

    				if (returnValue == RETCODE_OK)
    				{
						BSP_LED_Switch((uint32_t) BSP_XDK_LED_Y, (uint32_t) BSP_LED_COMMAND_OFF);
						BSP_LED_Switch((uint32_t) BSP_XDK_LED_O, (uint32_t) BSP_LED_COMMAND_OFF);
						BSP_LED_Switch((uint32_t) BSP_XDK_LED_R, (uint32_t) BSP_LED_COMMAND_OFF);
    				}
    			}
    			break;
    			case WAIT_FOR_SYNC:
    			{
    				switch (currentOpState)
    				{
    					case INITIALIZED:
    					{
    						printf("INITIALIZED -> WAIT_FOR_SYNC - Entering SLAVE MODE \n\r ");
    						opState = WAIT_FOR_SYNC;
    						returnValue = RETCODE_OK;
    					}
    					break;
    					default:
    					{
    						/* CANNOT TRANSITION BACK TO SLAVE MODE */
    						transitionTo(FAILED_TO_SYNC);
    						returnValue = RETCODE(RETCODE_SEVERITY_ERROR, RETCODE_FAILURE);
    					}
    					break;
    				}

    				if (returnValue == RETCODE_OK)
    				{
						BSP_LED_Switch((uint32_t) BSP_XDK_LED_Y, (uint32_t) BSP_LED_COMMAND_OFF);
						BSP_LED_Switch((uint32_t) BSP_XDK_LED_O, (uint32_t) BSP_LED_COMMAND_ON);
						BSP_LED_Switch((uint32_t) BSP_XDK_LED_R, (uint32_t) BSP_LED_COMMAND_OFF);

						master = 0;
    				}
    			}
    			break;
    			case WAIT_FOR_DELAY_REQ:
    			{
    				switch (currentOpState)
    				{
    					case INITIALIZED:
    					{
    						printf("INITIALIZED -> WAIT_FOR_DELAY_REQ - Entering MASTER MODE \n\r ");
    						opState = WAIT_FOR_DELAY_REQ;
    						returnValue = RETCODE_OK;
    					}
    					break;
    					case WAIT_FOR_SYNC:
    					{
    						printf("WAIT_FOR_SYNC -> WAIT_FOR_DELAY_REQ - Switching to MASTER MODE \n\r ");
    						opState = WAIT_FOR_DELAY_REQ;
    						returnValue = RETCODE_OK;
    					}
    					break;
    					case SYNCED:
    					{
    						printf("SYNCED -> WAIT_FOR_DELAY_REQ - Entering MASTER MODE again \n\r ");
    						opState = WAIT_FOR_DELAY_REQ;
    						returnValue = RETCODE_OK;
    					}
    					break;
    					default:
    					{
    						/* CANNOT TRANSITION TO MASTER MODE WHILE SYNCING*/
    						transitionTo(FAILED_TO_SYNC);
    						returnValue = RETCODE(RETCODE_SEVERITY_ERROR, RETCODE_FAILURE);
    					}
    					break;
    				}

    				if (returnValue == RETCODE_OK)
    				{
						BSP_LED_Switch((uint32_t) BSP_XDK_LED_Y, (uint32_t) BSP_LED_COMMAND_ON);
						BSP_LED_Switch((uint32_t) BSP_XDK_LED_O, (uint32_t) BSP_LED_COMMAND_OFF);
						BSP_LED_Switch((uint32_t) BSP_XDK_LED_R, (uint32_t) BSP_LED_COMMAND_OFF);

						master = 1;

						EnqueueDatatoWifi(SYNC);
    				}
    			}
    			break;
    			case WAIT_FOR_DELAY_RESP:
    			{
    				switch (currentOpState)
    				{
    					case WAIT_FOR_SYNC:
    					{
    						printf("WAIT_FOR_SYNC -> WAIT_FOR_DELAY_RESP - Synching... \n\r ");
    						opState = WAIT_FOR_DELAY_RESP;
    						returnValue = RETCODE_OK;
    					}
    					break;
    					default:
    					{
    						/* CANNOT SKIP SYNC MESSAGE*/
    						transitionTo(FAILED_TO_SYNC);
    						returnValue = RETCODE(RETCODE_SEVERITY_ERROR, RETCODE_FAILURE);
    					}
    					break;
    				}

    				if (returnValue == RETCODE_OK)
    				{
						BSP_LED_Switch((uint32_t) BSP_XDK_LED_Y, (uint32_t) BSP_LED_COMMAND_ON);
						BSP_LED_Switch((uint32_t) BSP_XDK_LED_O, (uint32_t) BSP_LED_COMMAND_ON);
						BSP_LED_Switch((uint32_t) BSP_XDK_LED_R, (uint32_t) BSP_LED_COMMAND_OFF);

						EnqueueDatatoWifi(DELAY_REQ);
    				}
    			}
    			break;
    			case WAIT_FOR_DELAY_ACK:
    			{
    				switch (currentOpState)
    				{
    					case WAIT_FOR_DELAY_REQ:
    					{
    						printf("WAIT_FOR_DELAY_REQ -> WAIT_FOR_DELAY_ACK - Synching... \n\r ");
    						opState = WAIT_FOR_DELAY_ACK;
    						returnValue = RETCODE_OK;
    					}
    					break;
    					default:
    					{
    						/* CANNOT SKIP SYNC MESSAGE*/
    						transitionTo(FAILED_TO_SYNC);
    						returnValue = RETCODE(RETCODE_SEVERITY_ERROR, RETCODE_FAILURE);
    					}
    					break;
    				}

    				if (returnValue == RETCODE_OK)
    				{
						BSP_LED_Switch((uint32_t) BSP_XDK_LED_Y, (uint32_t) BSP_LED_COMMAND_ON);
						BSP_LED_Switch((uint32_t) BSP_XDK_LED_O, (uint32_t) BSP_LED_COMMAND_ON);
						BSP_LED_Switch((uint32_t) BSP_XDK_LED_R, (uint32_t) BSP_LED_COMMAND_OFF);

						EnqueueDatatoWifi(DELAY_RESP);
    				}
    			}
    			break;
    			case SYNCED:
    			{
    				switch (currentOpState)
    				{
    					case WAIT_FOR_DELAY_RESP:
    					{
    						printf("WAIT_FOR_DELAY_RESP -> SYNCED - Synchronization SUCCEEDED \n\r ");
    						opState = SYNCED;
    						returnValue = RETCODE_OK;

    						EnqueueDatatoWifi(DELAY_ACK);
    					}
    					break;
    					case WAIT_FOR_DELAY_ACK:
    					{
    						printf("WAIT_FOR_DELAY_ACK -> SYNCED - Synchronization SUCCEEDED \n\r ");
    						opState = SYNCED;
    						returnValue = RETCODE_OK;
    					}
    					break;
    					default:
    					{
    						/* CANNOT SKIP SYNC PHASE*/
    						transitionTo(FAILED_TO_SYNC);
    						returnValue = RETCODE(RETCODE_SEVERITY_ERROR, RETCODE_FAILURE);
    					}
    					break;
    				}

    				if (returnValue == RETCODE_OK)
    				{
						BSP_LED_Switch((uint32_t) BSP_XDK_LED_Y, (uint32_t) BSP_LED_COMMAND_OFF);
						BSP_LED_Switch((uint32_t) BSP_XDK_LED_O, (uint32_t) BSP_LED_COMMAND_OFF);
						BSP_LED_Switch((uint32_t) BSP_XDK_LED_R, (uint32_t) BSP_LED_COMMAND_OFF);

					    printf(	"FINAL PTP MESSAGE {				\n\r 	.type			= %u	\n\r 	.validFields	= %u	\n\r 	.syncSendTime	= %u	\n\r 	.syncRecvTime	= %u	\n\r 	.dreqSendTime	= %u	\n\r 	.dreqRecvTime	= %u	\n\r 	.dresSendTime	= %u	\n\r 	.dresRecvTime	= %u	\n\r }							\n\r ",
								(unsigned int) syncData.type,
								(unsigned int) syncData.validFields,
								(unsigned int) syncData.syncSendTime,
								(unsigned int) syncData.syncRecvTime,
								(unsigned int) syncData.dreqSendTime,
								(unsigned int) syncData.dreqRecvTime,
								(unsigned int) syncData.dresSendTime,
								(unsigned int) syncData.dresRecvTime
						);

					    adjust = ((int32_t) syncData.syncRecvTime - (int32_t) syncData.syncSendTime - (int32_t) syncData.dreqRecvTime + (int32_t) syncData.dreqSendTime) / (int32_t) 2;

					    if (master == 1) {
					    	adjust *= -1;
					    }

					    /* correct only on positive side */
					    if (adjust < 0) {
					    	adjust = 0;
					    }

					    if (master == 1) {
						    partialTearDown();
						    dl_appInitSystem();
					    }
    				}
    			}
    			break;
    			case FAILED_TO_SYNC:
    			{
    				printf("... -> FAILED_TO_SYNC - An error occurred \n\r ");
    				opState = FAILED_TO_SYNC;
    				returnValue = RETCODE_OK;

					BSP_LED_Switch((uint32_t) BSP_XDK_LED_Y, (uint32_t) BSP_LED_COMMAND_OFF);
					BSP_LED_Switch((uint32_t) BSP_XDK_LED_O, (uint32_t) BSP_LED_COMMAND_OFF);
					BSP_LED_Switch((uint32_t) BSP_XDK_LED_R, (uint32_t) BSP_LED_COMMAND_ON);
    			}
    			break;
    			default:
    			{
    				printf("... -> ??? - Unrecognized destination Operational State, ingoring transition. \n\r ");
    				returnValue = RETCODE(RETCODE_SEVERITY_WARNING, RETCODE_INVALID_PARAM);
    			}
    			break;
    		}
    }


    if (pdTRUE != xSemaphoreGive(transitionSemaphore))
    {
        printf("Failed to release transition serialization semaphore \n\r ");
        returnValue = returnValue != RETCODE_OK ? returnValue : RETCODE(RETCODE_SEVERITY_ERROR, SEMAPHORE_GIVE_ERROR);
        transitionTo(FAILED_TO_SYNC);
    }

	if (returnValue != RETCODE_OK) {
		printf("Failed to process transition with error %u \n\r ", (unsigned int) returnValue);
	}
}

void transitionTo(OperationState state) {
	Retcode_T returnValue = CmdProcessor_enqueueFromIsr(AppCmdProcessorHandle, processTransitionTo, NULL, state);
	if (RETCODE_OK != returnValue)
	{
		printf("Transition failed with code: %u \n\r ", (unsigned int) returnValue);
	}
}

/**
 *
 */
static void processButton2Data(void * param1, uint32_t buttonstatus)
{
    BCDS_UNUSED(param1);

    switch (buttonstatus)
    {
		case BSP_XDK_BUTTON_PRESS:
			break;

		case BSP_XDK_BUTTON_RELEASE:
			transitionTo(WAIT_FOR_DELAY_REQ); /**< Entering MASTER MODE*/
			break;

		default:
			printf("FATAL Error: Unsolicited button event occurred for PB2 \n\r");
			transitionTo(FAILED_TO_SYNC);
			break;
    }
}

/**
 *
 */
void Button2Callback(uint32_t data)
{
    Retcode_T returnValue = CmdProcessor_enqueueFromIsr(AppCmdProcessorHandle, processButton2Data, NULL, data);
    if (RETCODE_OK != returnValue)
    {
        printf("Enqueuing for Button 2 callback failed\n\r");
    }
}

/**
 *  @brief       Function to print the XDK Device IP Address on the USB.
 *
 */
static void UpdateWithNetworkConfiguration(void)
{
    NetworkConfig_IpSettings_T myIpSettings;
    Ip_Address_T* IpaddressHex = Ip_getMyIpAddr();
    int32_t Result = INT32_C(-1);
    char ipAddress[UINT8_C(15)] = { 0 };

    memset(&myIpSettings, (uint32_t) ZERO, sizeof(myIpSettings));

    Retcode_T ReturnValue = NetworkConfig_GetIpSettings(&myIpSettings);
    if (ReturnValue == RETCODE_OK)
    {
        *IpaddressHex = Basics_htonl(myIpSettings.ipV4);
        Result = Ip_convertAddrToString(IpaddressHex, ipAddress);
        if (Result < INT32_C(0))
        {
            printf("Couldn't convert the IP address to string format \r\n ");
        }
        printf(" Ip address of the device %s \r\n ", ipAddress);

        *IpaddressHex = Basics_htonl(myIpSettings.ipV4Gateway);
        Result = Ip_convertAddrToString(IpaddressHex, ipAddress);
        if (Result < INT32_C(0))
        {
            printf("Couldn't convert the IP address to string format \r\n ");
        }
        printf(" Gateway Ip address of the device %s \r\n ", ipAddress);

        *IpaddressHex = Basics_htonl(myIpSettings.ipV4Mask);
        Result = Ip_convertAddrToString(IpaddressHex, ipAddress);
        if (Result < INT32_C(0))
        {
            printf("Couldn't convert the IP address to string format \r\n ");
        }
        printf(" Mask of the device %s \r\n ", ipAddress);

        syncDestination = Basics_htonl(myIpSettings.ipV4Gateway) | ( ~ Basics_htonl(myIpSettings.ipV4Mask) );
		Result = Ip_convertAddrToString(&syncDestination, ipAddress);
		if (Result < INT32_C(0))
		{
			printf("Couldn't convert the IP address to string format \r\n ");
		}
		printf(" Broadcast IP address for the device %s \r\n ", ipAddress);
    }
    else
    {
        printf("Error in getting IP settings\n\r");
    }
}

/**
 * @brief Callback function called on WIFI event
 *
 * @param [in]  Event : event to be send by WIFI during connection/disconnection.
 *
 *
 */
static void WlanEventCallback(WlanConnect_Status_T Event)
{
    switch (Event)
    {
    case WLAN_CONNECTED:
        printf("XDK Device Connected over WIFI \r\n");
        UpdateWithNetworkConfiguration();
        BindSocketForUdpRecv();
        xTaskCreate(wifiUdpRecv, "wifiUdpRecv", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY, &recvTask);
        transitionTo(WAIT_FOR_SYNC); /*< Entering SLAVE MODE by default */
        break;
    case WLAN_DISCONNECTED:
        printf("XDK Device disconnected form WIFI n/w \r\n");
        if (opState != SYNCED) {
        	transitionTo(FAILED_TO_SYNC);
        }
        break;
    case WLAN_CONNECTION_ERROR:
        printf("XDK Device WIFI Connection error \r\n");
        transitionTo(FAILED_TO_SYNC);
        break;
    case WLAN_CONNECTION_PWD_ERROR:
        printf("XDK Device WIFI connection error due to wrong password \r\n");
        transitionTo(FAILED_TO_SYNC);
        break;
    case WLAN_DISCONNECT_ERROR:
        printf("XDK Device WIFI Disconnect error \r\n");
        transitionTo(FAILED_TO_SYNC);
        break;
    default:
        printf("XDK Device unknown WIFI event \r\n");
        transitionTo(FAILED_TO_SYNC);
        break;
    }
}

/**
 * @brief        This function used to connect to wifi n/w with provided SSID & Password
 *
 * @retval       RETCODE_OK is API success or an error.
 */
static Retcode_T wifiConnect(void)
{
    WlanConnect_SSID_T 			connectSSID;
    WlanConnect_PassPhrase_T 	connectPassPhrase;
    Retcode_T 					returnVal = (Retcode_T) RETCODE_FAILURE;

    returnVal = WlanConnect_Init();

    if (RETCODE_OK != returnVal)
    {
        printf("Error occurred initializing WLAN \r\n ");
        return returnVal;
    }
    printf("Connecting to %s \r\n ", WLAN_CONNECT_WPA_SSID);

    connectSSID = (WlanConnect_SSID_T) WLAN_CONNECT_WPA_SSID;
    connectPassPhrase = (WlanConnect_PassPhrase_T) WLAN_CONNECT_WPA_PASS;
    returnVal = NetworkConfig_SetIpDhcp(NULL);
    if (RETCODE_OK != returnVal)
    {
        printf("Error in setting IP to DHCP\n\r");
        return returnVal;
    }
    returnVal = WlanConnect_WPA(connectSSID, connectPassPhrase, WlanEventCallback);
    if (RETCODE_OK != returnVal)
    {
        printf("Error occurred while connecting Wlan %s \r\n ", WLAN_CONNECT_WPA_SSID);
    }

    if (pdTRUE == xSemaphoreGive(wifiRadioSemaphore) && pdTRUE == xSemaphoreGive(wifiRadioSemaphore)) {
    	printf("Cannot make Wifi radio channels available \n\r ");
    	return RETCODE(RETCODE_SEVERITY_FATAL, SEMAPHORE_GIVE_ERROR);
    }

    return returnVal;
}

/**
 *  @brief      Initializes the required hardware and software: LEDs, Button 2, WLAN, Semaphores;
 *
 *  @retval     #RETCODE_OK when all the require HW components were correctly initialized
 *  @retval     #RETCODE_FAILURE otherwise
 */
static Retcode_T ptp_init(void)
{
	Retcode_T returnVal = RETCODE_OK;

	/* Initialize Semaphores */
    transitionSemaphore = xSemaphoreCreateBinary();
    if (NULL == transitionSemaphore)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, SEMAPHORE_CREATE_ERROR));
    }
    else
    {
        if (pdTRUE != xSemaphoreGive(transitionSemaphore))
        {
            printf("Initial release of transition serialization semaphore FAILED \n\r ");
            return RETCODE(RETCODE_SEVERITY_FATAL, SEMAPHORE_GIVE_ERROR);
        }
    }

    syncDataSemaphore = xSemaphoreCreateBinary();
    if (NULL == syncDataSemaphore)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, SEMAPHORE_CREATE_ERROR));
    }
    else
    {
        if (pdTRUE != xSemaphoreGive(syncDataSemaphore))
        {
            printf("Initial release of syncData serialization semaphore FAILED \n\r ");
            return RETCODE(RETCODE_SEVERITY_FATAL, SEMAPHORE_GIVE_ERROR);
        }
    }

    wifiRadioSemaphore = xSemaphoreCreateCounting(2,2);
    if (NULL == wifiRadioSemaphore)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, SEMAPHORE_CREATE_ERROR));
    }

	/* Initialize LEDs */
//	if (returnVal == RETCODE_OK)
//	{
//		printf("Initializing LEDs... \n\r ");
//		returnVal = BSP_LED_Connect();
//	}
//	if (returnVal == RETCODE_OK)
//	{
//		returnVal = BSP_LED_Enable((uint32_t) BSP_XDK_LED_Y);
//	}
//	if (returnVal == RETCODE_OK)
//	{
//		returnVal = BSP_LED_Enable((uint32_t) BSP_XDK_LED_O);
//	}
//	if (returnVal == RETCODE_OK)
//	{
//		returnVal = BSP_LED_Enable((uint32_t) BSP_XDK_LED_R);
//	}

	/* Initialize Buttons */
	if (returnVal == RETCODE_OK)
	{
		printf("Initializing Buttons... \n\r ");
		returnVal = BSP_Button_Connect();
	}
	if (returnVal == RETCODE_OK)
	{
		returnVal = BSP_Button_Enable((uint32_t) BSP_XDK_BUTTON_2, Button2Callback);
	}

	/* Initialize WLAN */
	if (returnVal == RETCODE_OK)
	{
		printf("Initializing WLAN... \n\r ");
		returnVal = wifiConnect();
	}

	return returnVal;
}

/**
 * TODO - Delete after testing
 */
static void myTimerCallback(xTimerHandle xTimer)
{
	(void) xTimer;
	TickType_t currentTime =  xTaskGetTickCount() - adjust;
	if (currentTime % SECONDS(5) == 0) {
		printf("TICK %u\n\r", (unsigned int) currentTime);
	}
}

/* global error flags to set by incorrect sensor init*/
/* inline functions ********************************************************* */

/**
 * @brief This is a template function where the user can write his custom application.
 *
 */

void appInitSystem(void * CmdProcessorHandle, uint32_t param2)
{
    if (CmdProcessorHandle == NULL)
    {
        printf("Command processor handle is null \n\r");
        assert(false);
    }
    BCDS_UNUSED(param2);
    AppCmdProcessorHandle = CmdProcessorHandle;

    // TODO - BEGIN Delete after testing
    xTimerHandle timerHandle = xTimerCreate(
    		(const char * const) "My Timer", // used only for debugging purposes
			MILLISECONDS(1), // timer period
			pdTRUE, //Autoreload pdTRUE or pdFALSE - should the timer start again
			//after it expired?
			NULL, // optional identifier
			myTimerCallback // pointer to a static callback function
    );
    if(NULL == timerHandle) {
    	assert(pdFAIL);
    	return;
    }
    BaseType_t timerResult = xTimerStart(timerHandle, MILLISECONDS(10));
    if(pdTRUE != timerResult) {
    	assert(pdFAIL);
    }
    // TODO - END Delete after testing

    Retcode_T retcode = ptp_init();
    if (retcode == RETCODE_OK)
    {
    	printf("Initialization completed OK \n\r");
    	transitionTo(INITIALIZED);
    }
    else
    {
    	printf("Initialization failed with error %u \n\r", (unsigned int) retcode);
    	transitionTo(FAILED_TO_SYNC);
    }
}

/* local init-functions including System init and all Timer******************** */
extern void init(void)
{
    Retcode_T sdInitreturnValue;
    Retcode_T returnVal = RETCODE_OK;
    SDC_sdCardAppReturn_t csuReturnValue = 0;
    FRESULT fileSystemResult = 0;
    TaskHandle_t xHandle = NULL;
    TaskHandle_t xHandle1 = NULL;
    TaskHandle_t xHandle2 = NULL;
    const char *pcTextForTask1 = "TASK1 IS RUNNING";
    const char *pcTextForTask2 = "TASK2 IS RUNNING";
    const char *pcTextForTask3 = "TASK3 IS RUNNING";
    FILINFO initfno;
    /* Initialize SD card */
    sdInitreturnValue = SDCardDriver_Initialize();
    ReadSensorSemaphor = xSemaphoreCreateMutex();
    if (ReadSensorSemaphor == NULL)
    {
        assert(0);
    }
    ActiveBuffer = &pingBuffer;
    BackBuffer = &pongBuffer;
    if (sdInitreturnValue != RETCODE_OK)
    { /* Debug fail case test SDC Init */
        assert(0);
    }
    else
    {
        if (SDC_diskInitStatus != SDCARD_DISK_INITIALIZED) /*SD-Card Disk Not Initialized */
        {
            SDC_diskInitStatus = SDCardDriver_DiskInitialize(SDC_DRIVE_ZERO); /* Initialize SD card */
        }
        if (SDCARD_DISK_INITIALIZED == SDC_diskInitStatus)
        {
            if (f_mount(&globalmnt, DEFAULT_LOGICAL_DRIVE, FORCE_MOUNT) != FR_OK)
            {
                assert(0);
            }
		}
	   else {
		   assert(0);
        }
    }
    if (SDC_APP_ERR_ERROR == csuReturnValue)
    { /* Debug fail case test for SDC Read/Write */
        assert(0);
    }
    else
    {
        getIniValues(); /**< get Sensor configuration*/
        if (FR_OK != f_stat("logger.ini", &initfno))
        {
            missingFile = 1;
        }
        if (strcmp(config.fileformat, "custom") == 0)
        {
            /**< search for custlog.ini on Sd-Card*/
            if (FR_OK == f_stat("custlog.ini", &initfno))
            {
                if (Count_CustLogLines() == 2) /**< only read the custlog lines if lines == 2*/
                {
                    customLog_LineRead(customHeader, custstring);
                }
                else
                {
                    returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
                    if (RETCODE_OK != returnVal)
                    {
                        printf("Turning on of RED LED failed");
                    }
                    strcpy(customHeader,
                            "custlog.ini not equals specification");
                    strcpy(custstring, "custlog.ini not equals specification");
                    printf("custlog.ini not equals specification\n");
                    exit(0);
                }
            }
            else
            {
                strcpy(customHeader, "CUSTLOG INI MISSING");
                strcpy(custstring, "CUSTLOG INI MISSING");
                missingFile = 1;
            }
        }
        else
        {
            customHeader[0] = '\0';
            custstring[0] = '\0';
        }
        scan_files(); /**< scan the files on SD-Card*/
        Sensor_init(); /**< Initialize the Sensors*/
        fileSystemResult = f_open(&fileObject, filename,
        FA_OPEN_EXISTING | FA_WRITE);
        if (fileSystemResult != FR_OK)
        {
            sprintf(filename, config.filename, buttoncount);
            fileSystemResult = f_open(&fileObject, filename,
            FA_CREATE_ALWAYS | FA_WRITE);
            if (fileSystemResult != FR_OK)
            {
                returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
                if (RETCODE_OK != returnVal)
                {
                    printf("Turning on of RED LED failed");
                }

                exit(0);
            }
        }
        fileSystemResult = f_lseek(&fileObject, f_size(&fileObject));
        if (fileSystemResult != FR_OK)
        {
            returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
            if (RETCODE_OK != returnVal)
            {
                printf("Turning on of RED LED failed");
            }

        }
        if (xTaskCreate(normal_blink, (const char * const) pcTextForTask2,
                configMINIMAL_STACK_SIZE, NULL, 4, &xHandle1) != pdTRUE)
        {
            assert(0);
        }
        if (xTaskCreate(SDC_writeBackBuffer, (const char * const) pcTextForTask3,
                configMINIMAL_STACK_SIZE, NULL, 4, &xHandle2) != pdTRUE)
        {
            assert(0);
        }
        if (xTaskCreate(UpdateSensorValues, (const char * const) pcTextForTask1,
                configMINIMAL_STACK_SIZE, NULL, 4, &xHandle) != pdTRUE)
        {
            assert(0);
        }
    }
}

void dl_appInitSystem(void)
{
	printf("Switching to datalogger mode\n\r");
    vTaskDelay(SECONDS(5));
    init();
}

/* API documentation is in the configuration header */
void PB0_Init(void)
{
    Retcode_T returnVal = RETCODE_OK;
    returnVal = BSP_Button_Connect();
    if (RETCODE_OK == returnVal)
    {
        returnVal = BSP_Button_Enable((uint32_t) BSP_XDK_BUTTON_1, PB0_InterruptCallback);
    }
}

extern void Sensor_init(void)
{
    uint32_t bytesPerSample = TIMESTAMP_MAXBYTES;
    uint32_t validNumberOfBytes = 0;
    uint32_t samplesPerSecond = 0;
    uint32_t enabledSensorCounter = 0;/**< count the enabled sensor to calculate the json overhead*/

    if (config.bma280_enabled == 1)
    {
        bma_280_init();
        bytesPerSample += BMA280_MAXBYTES; /**< add the max byte length of the BMA 280 sensor parameter*/
        enabledSensorCounter++;
    }

    if (config.bme280_enabled == 1)
    {
        bme_280_init();
        bytesPerSample += BME280_MAXBYTES; /**< add the max byte length of the BME 280 sensor parameter*/
        enabledSensorCounter++;
    }

    if (config.bmg160_enabled == 1)
    {
        bmg_160_init();
        bytesPerSample += BMG160_MAXBYTES; /**< add the max byte length of the BMG 160 sensor parameter*/
        enabledSensorCounter++;
    }

    if (config.bmi160_enabled == 1)
    {
        bmi_160_init();
        bytesPerSample += BMI160_MAXBYTES; /**< add the max byte length of the BMI 160 sensor parameter*/
        enabledSensorCounter += 2;
    }

    if (config.bmm150_enabled == 1)
    {
        bmm_150_init();
        bytesPerSample += BMM150_MAXBYTES; /**< add the max byte length of the BMM 150 sensor parameter*/
        enabledSensorCounter++;
    }

    if (config.max44009_enabled == 1)
    {
        max_44009_init();
        bytesPerSample += MAX44009_MAXBYTES; /**< add the max byte length of the MAX 44009 sensor parameter*/
        enabledSensorCounter++;
    }
    if (strcmp(conf->fileformat, "json") == 0)
    {
        bytesPerSample += enabledSensorCounter * JSONOVERHEAD_PER_SENSOR;
    }
    validNumberOfBytes = BYTESPERS / bytesPerSample; /**< max number of bytes referring to BYTES PER Second and the sampling rate in ms*/
    samplesPerSecond = (validNumberOfBytes / 10) * 10;

    /**< set the sampling rate according to the rounded value of samples per second*/
    if (samplesPerSecond > 300)
    {
        fastestSamplingRate = samplesPerSecond / 100 * 100;
    }
    else if (samplesPerSecond > 100)
    {
        fastestSamplingRate = samplesPerSecond / 50 * 50;
    }
    else
    {
        fastestSamplingRate = samplesPerSecond;
    }
    minimalTicks = 1000 / fastestSamplingRate; /**< set the minimal valid Tick rate*/
    if (minimalTicks < 2)
    {
        minimalTicks = 2;
    }
    /**< set configuration to minimalTicks*/
    if (config.bma280_sampling_rate > fastestSamplingRate)
    {
        config.bma280_sampling_rate_timer_ticks = minimalTicks;
        config.bma280_sampling_rate_remaining_ticks = minimalTicks;
    }

    if (config.bmg160_sampling_rate > fastestSamplingRate)
    {
        config.bmg160_sampling_rate_timer_ticks = minimalTicks;
        config.bmg160_sampling_rate_remaining_ticks = minimalTicks;
    }

    if (config.bmi160_sampling_rate > fastestSamplingRate)
    {
        config.bmi160_sampling_rate_timer_ticks = minimalTicks;
        config.bmi160_sampling_rate_remaining_ticks = minimalTicks;
    }

    if (config.bmm150_sampling_rate > fastestSamplingRate)
    {
        config.bmm150_sampling_rate_timer_ticks = minimalTicks;
        config.bmm150_sampling_rate_remaining_ticks = minimalTicks;
    }

    if (config.bme280_sampling_rate > fastestSamplingRate)
    {
        config.bme280_sampling_rate_timer_ticks = minimalTicks;
        config.bme280_sampling_rate_remaining_ticks = minimalTicks;
    }

    if (config.max44009_sampling_rate > fastestSamplingRate)
    {
        config.max44009_sampling_rate_timer_ticks = minimalTicks;
        config.max44009_sampling_rate_remaining_ticks = minimalTicks;
    }
    printf("Maximum sampling rate for this configuration: %ld Hz\n",
            fastestSamplingRate);
    printf("Maximum ticks for this configuration: %ld ms\n", minimalTicks);
    printf("BMA280 ticks %ld ms \n", config.bma280_sampling_rate_timer_ticks);
    printf("BMG160 ticks %ld ms\n", config.bmg160_sampling_rate_timer_ticks);
    printf("BMI160 ticks %ld ms\n", config.bmi160_sampling_rate_timer_ticks);
    printf("BMM150 ticks %ld ms\n", config.bmm150_sampling_rate_timer_ticks);
    printf("BME280 ticks %ld ms\n", config.bme280_sampling_rate_timer_ticks);
    printf("MAX ticks %ld ms\n", config.max44009_sampling_rate_timer_ticks);
    PB0_Init();
}

/* End of Init-Section********************************************************************************** */

/* All Callback function for Timer and Interrupt events like button pressed********************************* */
/* API documentation is in the configuration header */
void PB0_InterruptCallback(uint32_t data)
{
	/* Button 1 Released event , File number has to increase */
	if (BSP_XDK_BUTTON_RELEASED == data) {
		BackBuffer->length = 0;
		BackBuffer->data[0] = 0;
		ActiveBuffer->length = 0;
		ActiveBuffer->data[0] = 0;
		newFile = 1;
		addnewfile = 1;
		closefile = 1;
		buttoncount++;
		sprintf(filename, config.filename, buttoncount);
	}
}
/* API documentation is in the configuration header */
void normal_blink(void *pvParameters)
{
    Retcode_T returnVal = RETCODE_OK;
    int8_t sdc_status = 0; /**< variable to check the SDC-Status, set if no SD-Card is found*/
    int8_t led_sdc_status = 0; /**< variable to check the led-status by missing SD-Card blink interval*/
    int8_t missingfile_led = 0; /**< variable to check the led-status by missing INI-File blink interval*/
    int8_t btnpressed_led = 0; /**< variable to check the led-status by button pressed-event*/
    int8_t normal_led_status = 0; /**< variable to check the led-status by normal run-mode*/
    (void) pvParameters;
    for (;;)
    {
        if (SDCARD_INSERTED == SDCardDriver_GetDetectStatus())
        {
            sdc_status = 0;
            returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_OFF);
            if (RETCODE_OK == returnVal)
            {
                printf("Turning on of RED LED failed");
            }

        }
        else
        {
            sdc_status = 1;
        }
        if ((normal_led_status == 0) && (missingFile == 0) && (sdc_status == 0)
                && (addnewfile == 0))
        {
            returnVal = BSP_LED_Switch(BSP_XDK_LED_O, BSP_LED_COMMAND_ON);
            if (RETCODE_OK != returnVal)
            {
                printf("Turning on of ORANGE LED failed");
            }

            normal_led_status = 1;
            static_assert((portTICK_RATE_MS != 0), "Tick rate MS is zero");
            vTaskDelay((portTickType) 500 / portTICK_RATE_MS);
        }
        else if ((normal_led_status == 1) && (missingFile == 0)
                && (sdc_status == 0) && (addnewfile == 0))
        {
            returnVal = BSP_LED_Switch(BSP_XDK_LED_O, BSP_LED_COMMAND_OFF);
            if (RETCODE_OK != returnVal)
            {
                printf("Turning off of ORANGE LED failed");
            }

            normal_led_status = 0;
            static_assert((portTICK_RATE_MS != 0), "Tick rate MS is zero");
            vTaskDelay((portTickType) 3000 / portTICK_RATE_MS);
        }
        else if ((addnewfile == 1) && (btnpressed_led == 0))
        {
            returnVal = BSP_LED_Switch(BSP_XDK_LED_Y, BSP_LED_COMMAND_ON);
            if (RETCODE_OK != returnVal)
            {
                printf("Turning on of YELLOW LED failed");
            }

            btnpressed_led = 1;
            static_assert((portTICK_RATE_MS != 0), "Tick rate MS is zero");
            vTaskDelay((portTickType) 2000 / portTICK_RATE_MS);
        }
        else if ((addnewfile == 1) && (btnpressed_led == 1))
        {
            returnVal = BSP_LED_Switch(BSP_XDK_LED_Y, BSP_LED_COMMAND_OFF);
            if (RETCODE_OK != returnVal)
            {
                printf("Turning on of YELLOW LED failed");
            }

            btnpressed_led = 0;
            addnewfile = 0;
        }
        else if ((sdc_status == 1) && (led_sdc_status == 0))
        {
            returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
            if (RETCODE_OK != returnVal)
            {
                printf("Turning on of RED LED failed");
            }

            led_sdc_status = 1;
            static_assert((portTICK_RATE_MS != 0), "Tick rate MS is zero");
            vTaskDelay((portTickType) 500 / portTICK_RATE_MS);
        }
        else if ((sdc_status == 1) && (led_sdc_status == 1))
        {
            returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_OFF);
            if (RETCODE_OK != returnVal)
            {
                printf("Turning off of RED LED failed");
            }

            led_sdc_status = 0;
            static_assert((portTICK_RATE_MS != 0), "Tick rate MS is zero");
            vTaskDelay((portTickType) 500 / portTICK_RATE_MS);
        }
        else if ((missingFile == 1) && (missingfile_led == 0))
        {
            returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
            if (RETCODE_OK != returnVal)
            {
                printf("Turning on of RED LED failed");
            }

            missingfile_led = 1;
            static_assert((portTICK_RATE_MS != 0), "Tick rate MS is zero");
            vTaskDelay((portTickType) 1000 / portTICK_RATE_MS);
        }
        else if ((missingFile == 1) && (missingfile_led == 1))
        {
            returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_OFF);
            if (RETCODE_OK != returnVal)
            {
                printf("Turning off of RED LED failed");
            }

            missingfile_led = 0;
            static_assert((portTICK_RATE_MS != 0), "Tick rate MS is zero");
            vTaskDelay((portTickType) 1000 / portTICK_RATE_MS);
        }
    }
}

/* API documentation is in the configuration header */
void SDC_writeBackBuffer(void *pvParameters)
{
    Retcode_T returnVal = RETCODE_OK;
    (void) pvParameters;
    TickType_t xLastWakeTimeBufferWrite;
    const TickType_t xBufferWriteFrequency = SD_TASK_INTERVAL_IN_TICKS;
    int suceedwrite = 0;
    int fprintfret = 0;
    FRESULT fileSystemResult = 0;
    // Initialise the xLastWakeTime variable with the current time.
    xLastWakeTimeBufferWrite = xTaskGetTickCount();
    for (;;)
    {
        vTaskDelayUntil(&xLastWakeTimeBufferWrite, xBufferWriteFrequency);
        uint32_t Ticks = (portTickType) 1;
        if (Ticks != UINT32_MAX) /* Validated for portMAX_DELAY to assist the task to wait Infinitely (without timing out) */
        {
            Ticks /= portTICK_RATE_MS;
        }
        if ((xSemaphoreTake(ReadSensorSemaphor, (portTickType)Ticks)
                == pdTRUE))
        {
            if (suceedwrite == 1)
            {
                BackBuffer->length = 0;
                BackBuffer->data[0] = 0;
                if (ActiveBuffer == &pingBuffer)
                {
                    ActiveBuffer = &pongBuffer;
                    BackBuffer = &pingBuffer;
                }
                else
                {
                    ActiveBuffer = &pingBuffer;
                    BackBuffer = &pongBuffer;
                }
                suceedwrite = 0;
            }
            if (xSemaphoreGive(ReadSensorSemaphor) != pdTRUE)
            {
                assert(0);
            }
        }
        else
        {
            assert(0);
        }
        if (closefile == 0)
        {
            fprintfret = f_printf(&fileObject, BackBuffer->data);
            if (fprintfret != -1)
            {
                f_sync(&fileObject);
                suceedwrite = 1;
            }
        }
        else
        {
            newFile = 1;
            writeLogFooter(conf, &fileObject);
            BackBuffer->length = 0;
            BackBuffer->data[0] = 0;
            f_close(&fileObject);
            fileSystemResult = f_open(&fileObject, filename,
            FA_CREATE_ALWAYS | FA_WRITE);
            if (fileSystemResult != FR_OK)
            {
                returnVal = BSP_LED_Switch(BSP_XDK_LED_O, BSP_LED_COMMAND_ON);
                if (RETCODE_OK != returnVal)
                {
                    printf("Turning on of ORANGE LED failed");
                }

            }
            fileSystemResult = f_lseek(&fileObject, f_size(&fileObject));
            if (fileSystemResult != FR_OK)
            {
                returnVal = BSP_LED_Switch(BSP_XDK_LED_O, BSP_LED_COMMAND_ON);
                if (RETCODE_OK != returnVal)
                {
                    printf("Turning on of ORANGE LED failed");
                }

            }
            closefile = 0;
        }
    }
}

/* API documentation is in the configuration header */
void UpdateSensorValues(void *pvParameters)
{
    (void) pvParameters;
    TickType_t xLastWakeTimeSensorRead;
    TickType_t firstSampleTicks = 0;
    uint32_t serialNumber = 0;
    const TickType_t xSensorReadFrequency = SAMPLE_TASK_INTERVAL_IN_TICKS;
    int32_t buffstatus = 0;
    uint8_t valuesToWrite = 0;
    // Initialise the xLastWakeTime variable with the current time.
    xLastWakeTimeSensorRead = xTaskGetTickCount();
    for (;;)
    {
        vTaskDelayUntil(&xLastWakeTimeSensorRead, xSensorReadFrequency);
        valuesToWrite = sampleSensors(conf);
        if (valuesToWrite)
        {
            uint32_t Ticks = 1;

            if (Ticks != UINT32_MAX) /* Validated for portMAX_DELAY to assist the task to wait Infinitely (without timing out) */
            {
                Ticks /= portTICK_RATE_MS;
            }
            if ((xSemaphoreTake(ReadSensorSemaphor, Ticks)
                    == pdTRUE))
            {
                buffstatus = BUFFSIZE - (ActiveBuffer->length + JSONOVERHEAD);
                if (buffstatus > 0)
                {
                    if ((newFile == 1))
                    {
                        //firstSampleTicks = xLastWakeTimeSensorRead;
                        ActiveBuffer->length = 0;
                        ActiveBuffer->data[0] = 0;
                        writeLogHeader(conf, customHeader);
                        newFile = 0;
                        serialNumber = 0;
                    }
                    writeLogEntry(conf, custstring,
                            xLastWakeTimeSensorRead - firstSampleTicks - adjust,
                            serialNumber);
                    serialNumber++;
                }
                if (xSemaphoreGive(ReadSensorSemaphor) != pdTRUE)
                {
                    assert(0);
                }
            }
        }
    }
}

/* End of Callback-Section********************************************************************************** */

/* All functions that be needed for read out logger.ini custlog.ini an the Write process to SD-Card  */
/* API documentation is in the configuration header */

FRESULT scan_files(void)
{
    FRESULT res;
    FILINFO fno;
    DIR dir;
    int i = 0;
    char *fn, *p; /* This function assumes non-Unicode configuration */
    TCHAR newestfile[MAX_FILE_NAME_LENGTH] = { 0 }; /**< variable to store the filename of the newest file on SD-Card*/
    static char path[MAX_PATH_LENGTH] = "";  /**< variable to store root path to SD-Card*/
    long temp_filenumber = 0;
    long filenumber = 0;

    res = f_opendir(&dir, path); /* Open the directory */
    if (res == FR_OK)
    {
        i = strlen(path);
        for (;;)
        {
            res = f_readdir(&dir, &fno); /* Read a directory item */
            if (res != FR_OK || fno.fname[0] == 0)
                break; /* Break on error or end of dir */
            if (fno.fname[0] == '.')
                continue; /* Ignore dot entry */

            fn = fno.fname;
            if (fno.fattrib & AM_DIR)
            { /* It is a directory */
				sprintf(path + i, "/%s", fn);
				res = scan_files();
				path[i] = 0;
                if (res != FR_OK)
                {
                    break;
                }
            }
            else
            { /* It is a file. */
                /**< check for newer time stamp and a valid file format*/
                if (((strstr(fno.fname, "CSV") != NULL)
                        || (strstr(fno.fname, "CST") != NULL)
                        || (strstr(fno.fname, "JSN") != NULL)))
                {
                    strcpy(newestfile, fn);
                    p = newestfile;
                    while (*p)
                    { // While there are more characters to process...

                        if (isdigit((int) *p))
                        { // Upon finding a digit, ...
                            temp_filenumber = strtol(p, &p, 10); // Read a number, ...
                        }
                        else
                        { // Otherwise, move on to the next character.
                            p++;
                        }
                    }
                    if (temp_filenumber >= filenumber)
                    {
                        filenumber = temp_filenumber;
                        buttoncount = filenumber + 1;
                    }
                }
            }
        }
    }
    return res;
}

/* API documentation is in the configuration header */
int getIniValues(void)
{
    Retcode_T returnVal = RETCODE_OK;
    FRESULT ret = 0;
    uint8_t k;

    /* get the Sensor-Config-Parameters */
    /* Section [general] */
    ret = ini_gets("general", "filename", 0, config.filename, 13, "logger.ini");
    ret = ini_gets("general", "fileformat", "logger_ini_missing.txt",
            config.fileformat, 7, "logger.ini");
    ret = ini_gets("general", "dataformat", "logger_ini_missing.txt",
            config.dataformat, 5, "logger.ini");

    /* first get all strings for sensor config*/
    ret = ini_gets("bma280", "bandwidth", "logger_ini_missing.txt", bma280_bw,
            12, "logger.ini");
    ret = ini_gets("bmg160", "bandwidth", "logger_ini_missing.txt", bmg160_bw,
            12, "logger.ini");
    ret = ini_gets("bmi160", "bandwidth_accel", "logger_ini_missing.txt",
            bmi160_accel_bw, 12, "logger.ini");
    ret = ini_gets("bmi160", "bandwidth_gyro", "logger_ini_missing.txt",
            bmi160_gyro_bw, 12, "logger.ini");
    ret = ini_gets("bmm150", "data_rate", "logger_ini_missing.txt", bmm150_data,
            12, "logger.ini");
    ret = ini_gets("bme280", "oversampling", "logger_ini_missing.txt",
            bme280_os, 4, "logger.ini");
    ret = ini_gets("bme280", "filter_coefficient", "logger_ini_missing.txt",
            bme280_coeff, 4, "logger.ini");
    ret = ini_gets("MAX44009", "integration_time", "logger_ini_missing.txt",
            MAX44009_int, 4, "logger.ini");

    /* bma280 Sensor */
    config.bma280_enabled = ini_getl("bma280", "enabled", 0, "logger.ini");
    config.bma280_sampling_rate = ini_getl("bma280", "sampling_rate", 0,
            "logger.ini");
    config.bma280_sampling_rate_timer_ticks = (1000
            / (ini_getl("bma280", "sampling_rate", 0, "logger.ini")));
    config.bma280_range = ini_getl("bma280", "range", 0, "logger.ini");
    config.bma280_bandwidth = ini_getl("bma280", "bandwidth", 0, "logger.ini");
    config.bma280_sampling_rate_remaining_ticks =
            config.bma280_sampling_rate_timer_ticks;

    if (fastestSamplingRate < config.bma280_sampling_rate)
    {
        fastestSamplingRate = config.bma280_sampling_rate;
    }

    fprintf(stdout,
            " config.bma280_enabled: %ld\n config.bma280_bandwidth: %ld\n config.bma280_range: %ld\n config.bma280_sampling_rate_timer_ticks: %ld\n \n",
            config.bma280_enabled, config.bma280_bandwidth, config.bma280_range,
            config.bma280_sampling_rate_timer_ticks);

    /* bmg160 Sensor */
    config.bmg160_enabled = ini_getl("bmg160", "enabled", 0, "logger.ini");
    config.bmg160_sampling_rate = ini_getl("bmg160", "sampling_rate", 0,
            "logger.ini");
    config.bmg160_sampling_rate_timer_ticks = (1000
            / (ini_getl("bmg160", "sampling_rate", 0, "logger.ini")));
    config.bmg160_bandwidth = ini_getl("bmg160", "bandwidth", 0, "logger.ini");
    config.bmg160_sampling_rate_remaining_ticks =
            config.bmg160_sampling_rate_timer_ticks;
    if (fastestSamplingRate < config.bmg160_sampling_rate)
    {
        fastestSamplingRate = config.bmg160_sampling_rate;
    }
    fprintf(stdout,
            " config.bmg160_enabled: %ld\n config.bmg160_bandwidth: %ld\n config.bmg160_sampling_rate_timer_ticks: %ld\n \n",
            config.bmg160_enabled, config.bmg160_bandwidth,
            config.bmg160_sampling_rate_timer_ticks);

    /* bmi160 */
    config.bmi160_enabled = ini_getl("bmi160", "enabled", 0, "logger.ini");
    config.bmi160_sampling_rate = ini_getl("bmi160", "sampling_rate", 0,
            "logger.ini");
    config.bmi160_sampling_rate_timer_ticks = (1000
            / (ini_getl("bmi160", "sampling_rate", 0, "logger.ini")));
    config.bmi160_bandwidth_accel = ini_getl("bmi160", "bandwidth_accel", 0,
            "logger.ini");
    config.bmi160_bandwidth_gyro = ini_getl("bmi160", "bandwidth_gyro", 0,
            "logger.ini");
    config.bmi160_range = ini_getl("bmi160", "range", 0, "logger.ini");
    config.bmi160_sampling_rate_remaining_ticks =
            config.bmi160_sampling_rate_timer_ticks;
    if (fastestSamplingRate < config.bmi160_sampling_rate)
    {
        fastestSamplingRate = config.bmi160_sampling_rate;
    }
    fprintf(stdout,
            " config.bmi160_enabled: %ld\n config.bmi160_bandwidth_accel: %ld\n config.bmi160_bandwidth_gyro: %ld\n config.bmi160_range: %ld\n config.bmi160_sampling_rate_timer_ticks: %ld\n \n",
            config.bmi160_enabled, config.bmi160_bandwidth_accel,
            config.bmi160_bandwidth_gyro, config.bmi160_range,
            config.bma280_sampling_rate_timer_ticks);

    /* bmm150 Sensor */
    config.bmm150_enabled = ini_getl("bmm150", "enabled", 0, "logger.ini");
    config.bmm150_sampling_rate = ini_getl("bmm150", "sampling_rate", 0,
            "logger.ini");
    config.bmm150_sampling_rate_timer_ticks = (1000
            / (ini_getl("bmm150", "sampling_rate", 0, "logger.ini")));
    config.bmm150_data_rate = ini_getl("bmm150", "data_rate", 0, "logger.ini");
    config.bmm150_sampling_rate_remaining_ticks =
            config.bmm150_sampling_rate_timer_ticks;
    if (fastestSamplingRate < config.bmm150_sampling_rate)
    {
        fastestSamplingRate = config.bmm150_sampling_rate;
    }
    fprintf(stdout,
            " config.bmm150_enabled: %ld\n config.bmm150_data_rate: %ld\n config.bmm150_sampling_rate_timer_ticks: %ld\n \n",
            config.bmm150_enabled, config.bmm150_data_rate,
            config.bmm150_sampling_rate_timer_ticks);

    /* bme280 sensor */
    config.bme280_enabled = ini_getl("bme280", "enabled", 0, "logger.ini");
    config.bme280_sampling_rate = ini_getl("bme280", "sampling_rate", 0,
            "logger.ini");
    config.bme280_sampling_rate_timer_ticks = (1000
            / (ini_getl("bme280", "sampling_rate", 0, "logger.ini")));
    config.bme280_oversampling = ini_getl("bme280", "oversampling", 0,
            "logger.ini");
    config.bme280_filter_coefficient = ini_getl("bme280", "filter_coefficient",
            0, "logger.ini");
    config.bme280_sampling_rate_remaining_ticks =
            config.bme280_sampling_rate_timer_ticks;
    if (fastestSamplingRate < config.bme280_sampling_rate)
    {
        fastestSamplingRate = config.bme280_sampling_rate;
    }
    fprintf(stdout,
            " config.bme280_enabled: %ld\n config.bme280_oversampling: %ld\n config.bme280_filter_coefficient: %ld\n config.bme280_sampling_rate_timer_ticks: %ld\n \n",
            config.bma280_enabled, config.bme280_oversampling,
            config.bme280_filter_coefficient,
            config.bme280_sampling_rate_timer_ticks);

    /* max4409 sensor */
    config.max44009_enabled = ini_getl("MAX44009", "enabled", 0, "logger.ini");
    config.max44009_sampling_rate = ini_getl("MAX44009", "sampling_rate", 0,
            "logger.ini");
    config.max44009_sampling_rate_timer_ticks = (1000
            / (ini_getl("MAX44009", "sampling_rate", 0, "logger.ini")));
    config.max44009_integration_time = ini_getl("MAX44009", "integration_time",
            0, "logger.ini");
    config.max44009_sampling_rate_remaining_ticks =
            config.max44009_sampling_rate_timer_ticks;
    if (fastestSamplingRate < config.max44009_sampling_rate)
    {
        fastestSamplingRate = config.max44009_sampling_rate;
    }
    fprintf(stdout,
            " config.max44009_enabled: %ld\n config.max44009_integration_time: %ld\n config.max44009_sampling_rate_timer_ticks: %ld\n \n",
            config.max44009_enabled, config.max44009_integration_time,
            config.max44009_sampling_rate_timer_ticks);

    /*copy filename from config to process variable filename */
    for (k = 0; k < 13; k++)
    {
        filename[k] = config.filename[k];
    }
    if (strlen(filename) == 0)
    {
        returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
        if (RETCODE_OK != returnVal)
        {
           printf("Turning on of RED LED failed");
        }

        printf("bad filename in logger.ini\n");
        exit(0);
    }
    return ret;
}

/* API documentation is in the configuration header */
int Count_CustLogLines()
{
    Retcode_T returnVal = RETCODE_OK;
    TCHAR CharBuffer[256] = { 0 };
    FIL FileObject;
    int lines = 0; /**< variable to store the amount of lines in custlog.ini**/

    if (f_open(&FileObject, "custlog.ini", FA_OPEN_EXISTING | FA_READ)
            == FR_OK)
    {
        while ((f_eof(&FileObject) == 0)) /**< get line by line until EOF*/
        {
            f_gets((char*) CharBuffer, sizeof(CharBuffer) / sizeof(TCHAR),
                    &FileObject);
            lines++; /**< increase lines on every line*/
        }
        f_close(&FileObject);
    }
    else
    {
        returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
        if (RETCODE_OK != returnVal)
        {
            printf("Turning on of RED LED failed");
        }

    }
    return lines;
}

/* API documentation is in the configuration header */
int customLog_LineRead(TCHAR customHeader[], TCHAR custstring[])
{
    Retcode_T returnVal = RETCODE_OK;
    FIL cntObject; /* File objects */
    int l;
    int retval = 0;

    if (f_open(&cntObject, "custlog.ini", FA_OPEN_EXISTING | FA_READ)
            == FR_OK)
    {
        for (l = 0; (f_eof(&cntObject) == 0); l++)
        {
            if (l == 0) /* first line*/
            {
                f_gets(customHeader, CUSTLOGBUFSIZE, &cntObject); /**< get first line*/
            }
            if (l == 1) /* second line*/
            {
                f_gets(custstring, CUSTLOGBUFSIZE, &cntObject);/**< get second line*/
            }
        }
        f_close(&cntObject);
    }
    else
    {
        returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
        if (RETCODE_OK != returnVal)
        {
            printf("Turning on of RED LED failed");
        }
        retval = 1;
    }
    return retval;
}

/* API documentation is in the configuration header */
uint8_t sampleSensors(configuration *conf)
{
    uint8_t writeValues = 0;
    /* count down the the sampling_rate_remaining_ticks of every enabled Sensor
     * the sampling_rate_remaining_ticks according to the sampling_rate_timer_ticks which gets a value from 1 to 1000*/
    if (conf->bma280_enabled == 1)
    {
        conf->bma280_sampling_rate_remaining_ticks--;
        if (conf->bma280_sampling_rate_remaining_ticks < 1)
        {
            conf->bma280_sampling_rate_remaining_ticks =
                    conf->bma280_sampling_rate_timer_ticks;
            bma280_getSensorValues(NULL);
            writeValues = 1;
        }
    }

    if (conf->bmg160_enabled == 1)
    {
        conf->bmg160_sampling_rate_remaining_ticks--;
        if (conf->bmg160_sampling_rate_remaining_ticks < 1)
        {
            conf->bmg160_sampling_rate_remaining_ticks =
                    conf->bmg160_sampling_rate_timer_ticks;
            bmg160_getSensorValues(NULL);
            writeValues = 1;
        }
    }

    if (conf->bmi160_enabled == 1)
    {
        conf->bmi160_sampling_rate_remaining_ticks--;
        if (conf->bmi160_sampling_rate_remaining_ticks < 1)
        {
            conf->bmi160_sampling_rate_remaining_ticks =
                    conf->bmi160_sampling_rate_timer_ticks;
            bmi160_getSensorValues(NULL);
            writeValues = 1;
        }
    }

    if (conf->bmm150_enabled == 1)
    {
        conf->bmm150_sampling_rate_remaining_ticks--;
        if (conf->bmm150_sampling_rate_remaining_ticks < 1)
        {
            conf->bmm150_sampling_rate_remaining_ticks =
                    conf->bmm150_sampling_rate_timer_ticks;
            bmm150_getSensorValues(NULL);
            writeValues = 1;
        }
    }

    if (conf->bme280_enabled == 1)
    {
        conf->bme280_sampling_rate_remaining_ticks--;
        if (conf->bme280_sampling_rate_remaining_ticks < 1)
        {
            conf->bme280_sampling_rate_remaining_ticks =
                    conf->bme280_sampling_rate_timer_ticks;
            bme280_getSensorValues(NULL);
            writeValues = 1;
        }
    }

    if (conf->max44009_enabled == 1)
    {
        conf->max44009_sampling_rate_remaining_ticks--;
        if (conf->max44009_sampling_rate_remaining_ticks < 1)
        {
            conf->max44009_sampling_rate_remaining_ticks =
                    conf->max44009_sampling_rate_timer_ticks;
            max44009_getSensorValues(NULL);
            writeValues = 1;
        }
    }
    return writeValues;
}

/* API documentation is in the configuration header */
char * stringReplace(char *search, char *replace, char *string)
{
    char *searchStart;
    char tempString[STRINGREPLACEBUFFER] = { 0 };
    int len = 0;
    uint32_t remainingBufSize = STRINGREPLACEBUFFER - strlen(string);
    /** check for search string
     */
    searchStart = strstr(string, search);
    if (searchStart == NULL)
    {
        return string;
    }
    /** temporary copy of the string
     */
    if (remainingBufSize >= strlen(replace))
    {
        strcpy((char *) tempString, string);

        /** set first section of the string
         */
        len = searchStart - string;
        string[len] = '\0';
        /** append second section of the string
         */
        strcat(string, replace);

        /** append third section of the string
         */
        len += strlen(search);
        strcat(string, (char*) tempString + len);
        return string;
    }
    else
    {
        return NULL;
    }
}

/* API documentation is in the configuration header */
void writeSensorDataCsvHeader(configuration *conf)
{
    if (strcmp(conf->dataformat, "raw") == 0)
    {
        ActiveBuffer->length += sprintf(
                ActiveBuffer->data + ActiveBuffer->length,
                " raw--timestamp[ms]");

        if (conf->bma280_enabled == 1)
        {
            ActiveBuffer->length += sprintf(
                    ActiveBuffer->data + ActiveBuffer->length,
                    ";bma280_x;bma280_y;bma280_z");
        }

        if (conf->bmg160_enabled == 1)
        {
            ActiveBuffer->length += sprintf(
                    ActiveBuffer->data + ActiveBuffer->length,
                    ";bmg160_x;bmg160_y;bmg160_z");
        }

        if (conf->bmi160_enabled == 1)
        {
            ActiveBuffer->length += sprintf(
                    ActiveBuffer->data + ActiveBuffer->length,
                    ";bmi160_a_x;bmi160_a_y;bmi160_a_z");
            ActiveBuffer->length += sprintf(
                    ActiveBuffer->data + ActiveBuffer->length,
                    ";bmi160_g_x;bmi160_g_y;bmi160_g_z");
        }

        if (conf->bmm150_enabled == 1)
        {
            ActiveBuffer->length += sprintf(
                    ActiveBuffer->data + ActiveBuffer->length,
                    ";bmm150_x;bmm150_y;bmm150_z;bmm150_res");
        }

        if (conf->bme280_enabled == 1)
        {
            ActiveBuffer->length += sprintf(
                    ActiveBuffer->data + ActiveBuffer->length,
                    ";bme280_temp;bme280_press;bme280_hum");
        }

        if (conf->max44009_enabled == 1)
        {
            ActiveBuffer->length += sprintf(
                    ActiveBuffer->data + ActiveBuffer->length,
                    ";max44009_bright");
        }
    }
    else
    {
        ActiveBuffer->length += sprintf(
                ActiveBuffer->data + ActiveBuffer->length,
                " unit--timestamp[ms]");

        if (conf->bma280_enabled == 1)
        {
            ActiveBuffer->length += sprintf(
                    ActiveBuffer->data + ActiveBuffer->length,
                    ";bma280_x[mg];bma280_y[mg];bma280_z[mg]");
        }

        if (conf->bmg160_enabled == 1)
        {
            ActiveBuffer->length += sprintf(
                    ActiveBuffer->data + ActiveBuffer->length,
                    ";bmg160_x[mDeg];bmg160_y[mDeg];bmg160_z[mDeg]");
        }

        if (conf->bmi160_enabled == 1)
        {
            ActiveBuffer->length += sprintf(
                    ActiveBuffer->data + ActiveBuffer->length,
                    ";bmi160_a_x[mg];bmi160_a_y[mg];bmi160_a_z[mg]");
            ActiveBuffer->length += sprintf(
                    ActiveBuffer->data + ActiveBuffer->length,
                    ";bmi160_g_x[mDeg];bmi160_g_y[mDeg];bmi160_g_z[mDeg]");
        }

        if (conf->bmm150_enabled == 1)
        {
            ActiveBuffer->length +=
                    sprintf(ActiveBuffer->data + ActiveBuffer->length,
                            ";bmm150_x[microT];bmm150_y[microT];bmm150_z[microT];bmm150_res");
        }

        if (conf->bme280_enabled == 1)
        {
            ActiveBuffer->length += sprintf(
                    ActiveBuffer->data + ActiveBuffer->length,
                    ";bme280_temp[mDeg];bme280_press[Pa];bme280_hum[rh]");
        }

        if (conf->max44009_enabled == 1)
        {
            ActiveBuffer->length += sprintf(
                    ActiveBuffer->data + ActiveBuffer->length,
                    ";max44009_bright[mLux]");
        }
    }
    ActiveBuffer->length += sprintf(ActiveBuffer->data + ActiveBuffer->length,
            ";\n");
}

/* API documentation is in the configuration header */
void writeSensorDataCsv(uint64_t timestamp, configuration *conf)
{
    /* First check configured data format then write the data of all enabled sensors*/
    ActiveBuffer->length += sprintf(ActiveBuffer->data + ActiveBuffer->length,
            "%llu", timestamp);
    if (strcmp(conf->dataformat, "raw") == 0)
    {
        if (conf->bma280_enabled == 1)
        {
            ActiveBuffer->length += sprintf(
                    ActiveBuffer->data + ActiveBuffer->length, ";%ld;%ld;%ld",
                    getAccelDataRaw.xAxisData, getAccelDataRaw.yAxisData,
                    getAccelDataRaw.zAxisData);
        }

        if (conf->bmg160_enabled == 1)
        {
            ActiveBuffer->length += sprintf(
                    ActiveBuffer->data + ActiveBuffer->length, ";%ld;%ld;%ld",
                    getRawData.xAxisData, getRawData.yAxisData, getRawData.zAxisData);
        }
        if (conf->bmi160_enabled == 1)
        {
            ActiveBuffer->length += sprintf(
                    ActiveBuffer->data + ActiveBuffer->length, ";%ld;%ld;%ld",
                    getAccelDataRaw160.xAxisData, getAccelDataRaw160.yAxisData,
                    getAccelDataRaw160.zAxisData);
            ActiveBuffer->length += sprintf(
                    ActiveBuffer->data + ActiveBuffer->length, ";%ld;%ld;%ld",
                    getGyroDataRaw160.xAxisData, getGyroDataRaw160.yAxisData,
                    getGyroDataRaw160.zAxisData);
        }

        if (conf->bmm150_enabled == 1)
        {
            ActiveBuffer->length += sprintf(
                    ActiveBuffer->data + ActiveBuffer->length,
                    ";%ld;%ld;%ld;%d", getMagDataRaw.xAxisData,
                    getMagDataRaw.yAxisData, getMagDataRaw.zAxisData,
                    getMagDataRaw.resistance);
        }

        if (conf->bme280_enabled == 1)
        {
            ActiveBuffer->length += sprintf(
                    ActiveBuffer->data + ActiveBuffer->length, ";%ld;%ld;%ld",
                    bme280lsb.humidity, bme280lsb.pressure,
                    bme280lsb.temperature);
        }

        if (conf->max44009_enabled == 1)
        {
            ActiveBuffer->length += sprintf(
                    ActiveBuffer->data + ActiveBuffer->length, ";%d",
                    luxRawData);
        }
    }
    else
    {
        if (conf->bma280_enabled == 1)
        {
            ActiveBuffer->length += sprintf(
                    ActiveBuffer->data + ActiveBuffer->length, ";%ld;%ld;%ld",
                    getAccelDataUnit.xAxisData, getAccelDataUnit.yAxisData,
                    getAccelDataUnit.zAxisData);
        }

        if (conf->bmg160_enabled == 1)
        {
            ActiveBuffer->length += sprintf(
                    ActiveBuffer->data + ActiveBuffer->length, ";%ld;%ld;%ld",
                    getMdegData.xAxisData, getMdegData.yAxisData, getMdegData.zAxisData);
        }

        if (conf->bmi160_enabled == 1)
        {
            ActiveBuffer->length += sprintf(
                    ActiveBuffer->data + ActiveBuffer->length, ";%ld;%ld;%ld",
                    getAccelDataUnit160.xAxisData,
                    getAccelDataUnit160.yAxisData,
                    getAccelDataUnit160.zAxisData);
            ActiveBuffer->length += sprintf(
                    ActiveBuffer->data + ActiveBuffer->length, ";%ld;%ld;%ld",
                    getGyroDataConv160.xAxisData, getGyroDataConv160.yAxisData,
                    getGyroDataConv160.zAxisData);
        }

        if (conf->bmm150_enabled == 1)
        {
            ActiveBuffer->length += sprintf(
                    ActiveBuffer->data + ActiveBuffer->length,
                    ";%ld;%ld;%ld;%d", getMagDataUnit.xAxisData,
                    getMagDataUnit.yAxisData, getMagDataUnit.zAxisData,
                    getMagDataUnit.resistance);
        }

        if (conf->bme280_enabled == 1)
        {
            ActiveBuffer->length += sprintf(
                    ActiveBuffer->data + ActiveBuffer->length, ";%ld;%ld;%ld",
                    bme280s.temperature, bme280s.pressure, bme280s.humidity);
        }

        if (conf->max44009_enabled == 1)
        {
            ActiveBuffer->length += sprintf(
                    ActiveBuffer->data + ActiveBuffer->length, ";%ld",
                    milliLuxData);
        }
    }
    ActiveBuffer->length += sprintf(ActiveBuffer->data + ActiveBuffer->length,
            ";\n");
}

/* API documentation is in the configuration header */
void writeSensorDataJson(uint64_t timestamp, uint64_t serialnumber,
        configuration *conf)
{
    /* First check configured data format then write the data of all enabled sensors*/
    if (strcmp(conf->dataformat, "raw") == 0)
    {
        ActiveBuffer->length += sprintf(
                ActiveBuffer->data + ActiveBuffer->length, "\"%lld\":{\n",
                serialnumber);
        ActiveBuffer->length += sprintf(
                ActiveBuffer->data + ActiveBuffer->length,
                "\"Timestamp[ms]\":%lld,\n", timestamp);

        if (conf->bma280_enabled == 1)
        {
            ActiveBuffer->length +=
                    sprintf(ActiveBuffer->data + ActiveBuffer->length,
                            "\"bma280_x\":%ld,\n" "\"bma280_y\":%ld,\n" "\"bma280_z\":%ld,\n",
                            getAccelDataRaw.xAxisData,
                            getAccelDataRaw.yAxisData,
                            getAccelDataRaw.zAxisData);
        }

        if (conf->bmg160_enabled == 1)
        {
            ActiveBuffer->length +=
                    sprintf(ActiveBuffer->data + ActiveBuffer->length,
                            "\"bmg160_x\":%ld,\n" "\"bmg160_y\":%ld,\n" "\"bmg160_z\":%ld,\n",
                            getRawData.xAxisData, getRawData.yAxisData,
                            getRawData.zAxisData);
        }

        if (conf->bmi160_enabled == 1)
        {
            ActiveBuffer->length +=
                    sprintf(ActiveBuffer->data + ActiveBuffer->length,
                            "\"bmi160_a_x\":%ld,\n" "\"bmi160_a_y\":%ld,\n" "\"bmi160_a_z\":%ld,\n" "\"bmi160_g_x\":%ld,\n" "\"bmi160_g_y\":%ld,\n" "\"bmi160_g_z\":%ld,\n",
                            getAccelDataRaw160.xAxisData,
                            getAccelDataRaw160.yAxisData,
                            getAccelDataRaw160.zAxisData,
                            getGyroDataRaw160.xAxisData, getGyroDataRaw160.yAxisData,
                            getGyroDataRaw160.zAxisData);
        }

        if (conf->bmm150_enabled == 1)
        {
            ActiveBuffer->length +=
                    sprintf(ActiveBuffer->data + ActiveBuffer->length,
                            "\"bmm150_x\":%ld,\n" "\"bmm150_y\":%ld,\n" "\"bmm150_z\":%ld,\n",
                            getMagDataRaw.xAxisData, getMagDataRaw.yAxisData,
                            getMagDataRaw.zAxisData);
        }

        if (conf->bme280_enabled == 1)
        {
            ActiveBuffer->length +=
                    sprintf(ActiveBuffer->data + ActiveBuffer->length,
                            "\"bme280_hum\":%ld,\n" "\"bme280_press\":%ld,\n" "\"bme280_temp\":%ld,\n",
                            bme280lsb.humidity, bme280lsb.pressure,
                            bme280lsb.temperature);
        }

        if (conf->max44009_enabled == 1)
        {
            ActiveBuffer->length += sprintf(
                    ActiveBuffer->data + ActiveBuffer->length,
                    "\"max44009_bright\":%d,\n", luxRawData);
        }
    }
    else
    {
        ActiveBuffer->length += sprintf(
                ActiveBuffer->data + ActiveBuffer->length, "\"%lld\":{\n",
                serialnumber);
        ActiveBuffer->length += sprintf(
                ActiveBuffer->data + ActiveBuffer->length,
                "\"Timestamp[ms]\":%lld,\n", timestamp);

        if (conf->bma280_enabled == 1)
        {
            ActiveBuffer->length +=
                    sprintf(ActiveBuffer->data + ActiveBuffer->length,
                            "\"bma280_x[mg]\":%ld,\n" "\"bma280_y[mg]\":%ld,\n" "\"bma280_z[mg]\":%ld,\n",
                            getAccelDataUnit.xAxisData,
                            getAccelDataUnit.yAxisData,
                            getAccelDataUnit.zAxisData);
        }

        if (conf->bmg160_enabled == 1)
        {
            ActiveBuffer->length +=
                    sprintf(ActiveBuffer->data + ActiveBuffer->length,
                            "\"bmg160_x[mDeg]\":%ld,\n" "\"bmg160_y[mDeg]\":%ld,\n" "\"bmg160_z[mDeg]\":%ld,\n",
                            getMdegData.xAxisData, getMdegData.yAxisData,
                            getMdegData.zAxisData);
        }

        if (conf->bmi160_enabled == 1)
        {
            ActiveBuffer->length +=
                    sprintf(ActiveBuffer->data + ActiveBuffer->length,
                            "\"bmi160_a_x[mg]\":%ld,\n" "\"bmi160_a_y[mg]\":%ld,\n" "\"bmi160_a_z[mg]\":%ld,\n" "\"bmi160_g_x[mDeg]\":%ld,\n" "\"bmi160_g_y[mDeg]\":%ld,\n" "\"bmi160_g_z[mDeg]\":%ld,\n",
                            getAccelDataUnit160.xAxisData,
                            getAccelDataUnit160.yAxisData,
                            getAccelDataUnit160.zAxisData,
                            getGyroDataConv160.xAxisData, getGyroDataConv160.yAxisData,
                            getGyroDataConv160.zAxisData);
        }

        if (conf->bmm150_enabled == 1)
        {
            ActiveBuffer->length +=
                    sprintf(ActiveBuffer->data + ActiveBuffer->length,
                            "\"bmm150_x[microT]\":%ld,\n" "\"bmm150_y[microT]\":%ld,\n" "\"bmm150_z[microT]\":%ld,\n",
                            getMagDataUnit.xAxisData, getMagDataUnit.yAxisData,
                            getMagDataUnit.zAxisData);
        }

        if (conf->bme280_enabled == 1)
        {
            ActiveBuffer->length +=
                    sprintf(ActiveBuffer->data + ActiveBuffer->length,
                            "\"bme280_hum[rh]\":%ld,\n" "\"bme280_press[Pa]\":%ld,\n" "\"bme280_temp[mDeg]\":%ld,\n",
                            bme280s.humidity, bme280s.pressure,
                            bme280s.temperature);
        }

        if (conf->max44009_enabled == 1)
        {
            ActiveBuffer->length += sprintf(
                    ActiveBuffer->data + ActiveBuffer->length,
                    "\"max44009_bright[mLux]\":%ld,\n",
                    milliLuxData);
        }
    }
    ActiveBuffer->length += sprintf(ActiveBuffer->data + ActiveBuffer->length,
            "}\n");
}

/* API documentation is in the configuration header */
void writeSensorDataJsonHeader(configuration *conf)
{
    /*The Header for Json-Files is called after File is created*/
    if (strcmp(conf->dataformat, "raw") == 0)
    {
        ActiveBuffer->length += sprintf(
                ActiveBuffer->data + ActiveBuffer->length,
                "SensorValuesRaw:[\n");
    }
    else
    {
        ActiveBuffer->length += sprintf(
                ActiveBuffer->data + ActiveBuffer->length,
                "SensorValuesUnit:[\n");
    }
}

/* API documentation is in the configuration header */
void writeSensorDataJsonFooter(FIL *fileObject)
{
    /*The Footer for Json-Files is called when file process is closed*/
    f_printf(fileObject, "]");
}

/* API documentation is in the configuration header */
void writeSensorDataCustom(char *customstring, uint64_t timestamp,
        configuration *conf)
{
    char buffer[10] = "";
    /*Write the Sensor date as specified in the custlog.ini*/
    char tempCustomString[STRINGREPLACEBUFFER] = { 0 };
    strcpy(tempCustomString, customstring);
    itoa(timestamp, buffer);
    stringReplace("%timestamp", buffer, tempCustomString); /* replace the string "%timestamp" with value of buffer*/
    if (strcmp(conf->dataformat, "raw"))
    {
        if (conf->bma280_enabled == 1)
        {
            itoa(getAccelDataRaw.xAxisData, buffer);
            stringReplace("%bma280_x", buffer, tempCustomString);
            itoa(getAccelDataRaw.yAxisData, buffer);
            stringReplace("%bma280_y", buffer, tempCustomString);
            itoa(getAccelDataRaw.zAxisData, buffer);
            stringReplace("%bma280_z", buffer, tempCustomString);
        }

        if (conf->bmg160_enabled == 1)
        {
            itoa(getRawData.xAxisData, buffer);
            stringReplace("%bmg160_x", buffer, tempCustomString);
            itoa(getRawData.yAxisData, buffer);
            stringReplace("%bmg160_y", buffer, tempCustomString);
            itoa(getRawData.zAxisData, buffer);
            stringReplace("%bmg160_z", buffer, tempCustomString);
        }

        if (conf->bmi160_enabled == 1)
        {
            itoa(getAccelDataRaw160.xAxisData, buffer);
            stringReplace("%bmi160_a_x", buffer, tempCustomString);
            itoa(getAccelDataRaw160.yAxisData, buffer);
            stringReplace("%bmi160_a_y", buffer, tempCustomString);
            itoa(getAccelDataRaw160.zAxisData, buffer);
            stringReplace("%bmi160_a_z", buffer, tempCustomString);
            itoa(getGyroDataRaw160.xAxisData, buffer);
            stringReplace("%bmi160_g_x", buffer, tempCustomString);
            itoa(getGyroDataRaw160.yAxisData, buffer);
            stringReplace("%bmi160_g_y", buffer, tempCustomString);
            itoa(getGyroDataRaw160.zAxisData, buffer);
            stringReplace("%bmi160_g_z", buffer, tempCustomString);
        }

        if (conf->bmm150_enabled == 1)
        {
            itoa(getMagDataRaw.xAxisData, buffer);
            stringReplace("%bmm150_x", buffer, tempCustomString);
            itoa(getMagDataRaw.yAxisData, buffer);
            stringReplace("%bmm150_y", buffer, tempCustomString);
            itoa(getMagDataRaw.zAxisData, buffer);
            stringReplace("%bmm150_z", buffer, tempCustomString);
        }

        if (conf->bme280_enabled == 1)
        {
            itoa(bme280lsb.humidity, buffer);
            stringReplace("%bme280_h", buffer, tempCustomString);
            itoa(bme280lsb.pressure, buffer);
            stringReplace("%bme280_p", buffer, tempCustomString);
            itoa(bme280lsb.temperature, buffer);
            stringReplace("%bme280_t", buffer, tempCustomString);
        }

        if (conf->max44009_enabled == 1)
        {
            itoa(luxRawData, buffer);
            stringReplace("%max44009_bright", buffer, tempCustomString);
        }
    }
    else
    {
        if (conf->bma280_enabled == 1)
        {
            itoa(getAccelDataUnit.xAxisData, buffer);
            stringReplace("%bma280_x", buffer, tempCustomString);
            itoa(getAccelDataUnit.yAxisData, buffer);
            stringReplace("%bma280_y", buffer, tempCustomString);
            itoa(getAccelDataUnit.zAxisData, buffer);
            stringReplace("%bma280_z", buffer, tempCustomString);
        }

        if (conf->bmg160_enabled == 1)
        {
            itoa(getMdegData.xAxisData, buffer);
            stringReplace("%bmg160_x", buffer, tempCustomString);
            itoa(getMdegData.yAxisData, buffer);
            stringReplace("%bmg160_y", buffer, tempCustomString);
            itoa(getMdegData.zAxisData, buffer);
            stringReplace("%bmg160_z", buffer, tempCustomString);
        }

        if (conf->bmi160_enabled == 1)
        {
            itoa(getAccelDataUnit160.xAxisData, buffer);
            stringReplace("%bmi160_a_x", buffer, tempCustomString);
            itoa(getAccelDataUnit160.yAxisData, buffer);
            stringReplace("%bmi160_a_y", buffer, tempCustomString);
            itoa(getAccelDataUnit160.zAxisData, buffer);
            stringReplace("%bmi160_a_z", buffer, tempCustomString);
            itoa(getGyroDataConv160.xAxisData, buffer);
            stringReplace("%bmi160_g_x", buffer, tempCustomString);
            itoa(getGyroDataConv160.yAxisData, buffer);
            stringReplace("%bmi160_g_y", buffer, tempCustomString);
            itoa(getGyroDataConv160.zAxisData, buffer);
            stringReplace("%bmi160_g_z", buffer, tempCustomString);
        }

        if (conf->bmm150_enabled == 1)
        {
            itoa(getMagDataUnit.xAxisData, buffer);
            stringReplace("%bmm150_x", buffer, tempCustomString);
            itoa(getMagDataUnit.yAxisData, buffer);
            stringReplace("%bmm150_y", buffer, tempCustomString);
            itoa(getMagDataUnit.zAxisData, buffer);
            stringReplace("%bmm150_z", buffer, tempCustomString);
        }

        if (conf->bme280_enabled == 1)
        {
            itoa(bme280s.humidity, buffer);
            stringReplace("%bme280_h", buffer, tempCustomString);
            itoa(bme280s.pressure, buffer);
            stringReplace("%bme280_p", buffer, tempCustomString);
            itoa(bme280s.temperature, buffer);
            stringReplace("%bme280_t", buffer, tempCustomString);
        }

        if (conf->max44009_enabled == 1)
        {
            itoa(milliLuxData, buffer);
            stringReplace("%max44009_bright", buffer, tempCustomString);
        }
    }
    ActiveBuffer->length += sprintf(ActiveBuffer->data + ActiveBuffer->length,
            "%s\n", tempCustomString);
    memset(tempCustomString, 0, STRINGREPLACEBUFFER);
}

/* API documentation is in the configuration header */
void writeLogEntry(configuration *conf, char *customstring,
        uint32_t currentTicks, uint32_t serialnumber)
{
    static uint32_t numberOfTicks = TIMESTAMP_UNIT_IN_TICKS;
    uint32_t timestamp = 0;

    timestamp = ((currentTicks) / numberOfTicks); /*calculate the time stamp without the logStartTime-Offset*/
    /*check the configured file format an call the related write-function */
    if (strcmp(conf->fileformat, "csv") == 0)
    {
        writeSensorDataCsv(timestamp, conf);
    }
    else if (strcmp(conf->fileformat, "json") == 0)
    {
        writeSensorDataJson(timestamp, serialnumber, conf);
    }
    else if (strcmp(conf->fileformat, "custom") == 0)
    {
        writeSensorDataCustom(customstring, timestamp, conf);
    }
    else
    {
        printf("Error writing log entry: no valid file format found\n");
    }
}

/* API documentation is in the configuration header */
void writeLogHeader(configuration *conf, char *customheader)
{
    logActive = 0;

    if (strcmp(conf->fileformat, "csv") == 0)
    {
        writeSensorDataCsvHeader(conf);
    }
    else if (strcmp(conf->fileformat, "json") == 0)
    {
        writeSensorDataJsonHeader(conf);
    }
    else if (strcmp(conf->fileformat, "custom") == 0)
    {
        ActiveBuffer->length += sprintf(
                ActiveBuffer->data + ActiveBuffer->length, "%s\n",
                customheader);
    }
    else
    {
        ActiveBuffer->length += sprintf(
                ActiveBuffer->data + ActiveBuffer->length, "%s\n", "false");
    }
}

/* API documentation is in the configuration header */
void writeLogFooter(configuration *conf, FIL *fileObject)
{
    logActive = 0;

    if ((strcmp(conf->fileformat, "csv") == 0)
            || (strcmp(conf->fileformat, "custom") == 0))
    {
        /**nothing to do since no footer*/
    }
    else if (strcmp(conf->fileformat, "json") == 0)
    {
        writeSensorDataJsonFooter(fileObject);
    }
    else
    {
        printf("Error writing log footer: no valid file format found\n");
    }
}

/* End of user-Section********************************************************************************** */

/**
 *  @brief API to Deinitialize the PSD module
 */
extern void PSD_deinit(void)
{
    bma_280_deInit();
    max_44009_deInit();
    bme_280_deInit();
    bmg_160_deInit();
    bmi160_deInit();
    bmm_150_deInit();
}

/** ************************************************************************* */
